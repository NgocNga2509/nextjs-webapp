"use client"

import { Modal } from "antd"

export default function DeleteWarning({ isOpen, setIsOpen }: any) {
    const handleCancel = () => {
        setIsOpen(false)
    }
    return (
        <>
            <Modal
                open={isOpen}
                onCancel={handleCancel}
            >
                Bạn muốn xóa đơn cầm này ?
            </Modal>
        </>
    )
}