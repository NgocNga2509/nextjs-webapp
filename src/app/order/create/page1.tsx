"use client"
import { useEffect, useState } from "react";
import {
    Button,
    ConfigProvider,
    DatePicker,
    Form,
    Input,
    InputNumber,
    Select,
} from "antd";
import dayjs from "dayjs";
import TextArea from "antd/es/input/TextArea";
import locale from 'antd/es/date-picker/locale/vi_VN';
import viVN from "antd/lib/locale/vi_VN";
import 'dayjs/locale/vi';
import TheContent from "@/app/helpers/TheContent"
import { ConvertNextMonth, FormatAmountFloatToInt, FormatAmountIntToFloat } from "@/app/helpers/Helps";
import NewCustomerModal from "@/app/customer/modals/NewCustomer";
import ListCustomerModal from "@/app/customer/modals/ListCustomer";
import BaseURL from "@/app/api/BaseURL";
import CustomerInfo from "../components/CustomerInfo";
import moment from "moment";
import { toast } from "react-toastify";
interface Product {
    category_gold_id: string;
    weight_gold: string;
}
interface DetailProduct {
    category_id: string;
    count: number;
}
interface Category {
    category_id: "",
    category_name: ""
}
interface CategoryPercent {
    percent_id: "",
    percent: ""
}
interface Customer {
    customer_id: "",
    customer_name: "",
    cccd: "",
    day_cccd: "",
    address_cccd: "",
    gender: "",
    address: "",
    birth_date: "",
    phone: "",
    created_at: "",
    updated_at: ""
}
export default function CreateOrder() {
    const [subMenu, setSubMenu] = useState(true);
    const [isOpenModal, setIsOpenModal] = useState<boolean>(false);
    const [dataCustomer, setDataCustomer] = useState<Customer>();
    const [isSelect, setIsSelect] = useState<boolean>(false);
    const [isSelectNew, setIsSelectNew] = useState<boolean>(false);
    const [customerID, setCustomerID] =useState<number>(0)
    const [newCustomer, setNewCustomer] = useState<boolean>(false);
    const isSubmit = isSelect || isSelectNew ? "true" : "fasle";
    const [dataCate, setDataCate] = useState<Category[]>([]);
    const [dataCateGold, setDataCateGold] = useState<Category[]>([]);
    const [dataCatePercent, setDataCatePercent] = useState<CategoryPercent[]>([]);
    const [amount, setAmount] = useState<any>("");
    const [isSuccess, setIsSuccess] = useState<boolean>(false);
    const [selectedProducts, setSelectedProducts] = useState<Product[]>([]);
    const [selectedDetailProducts, setSelectedDetailProducts] = useState<DetailProduct[][]>([]);

    const [message, setMessage] = useState("");
    const [selectedDate, setSelectedDate] = useState<any>(null);
    const [formData, setFormData] = useState({
        product_name: "",
        estimated_price: "",
        price: 0,
        weight_goal: "",
        status: "0",
        order_date: moment(),
        return_date: moment().add(1, "month"),
        interest_paid_count: 0,
        category_id: "",
        category_goal_id: "",
        percent: "",
        customer_id: "",
        note: "",
    });
    //
    const originalDate = new Date();
    // Lấy các thông tin ngày, tháng, năm từ ngày ban đầu
    const day = originalDate.getDate();
    const month = originalDate.getMonth() + 1; // Tháng trong JavaScript bắt đầu từ 0, nên cần cộng thêm 1
    const year = originalDate.getFullYear();
    const [expandSidebar, setExpandSidebar] = useState(false)
    // Định dạng lại thành dd/mm/yyyy
    const formattedDate = `${year}-${month < 10 ? "0" + month : month}-${day < 10 ? "0" + day : day}`;
    //
    useEffect(() => {
        BaseURL.get("list-newest-customer").then((response) => {
            if (response.status == 200) {
                setDataCustomer(response.data)
            }

        })
    }, [isSelectNew])

    const handleChange = (e: any) => {
        setFormData({ ...formData, [e.target.name]: e.target.value });
    };
    const handleChangePercent = (e: any) => {
        setFormData({ ...formData, percent: e });
    }
    const handleAmountChange = (event: any) => {
        const value = event.target.value;
        console.log(value);
        const formattedValue = FormatAmountIntToFloat(value);
        setAmount(formattedValue);
        setFormData({ ...formData, price: value });
    };
    const handleAddProduct = () => {
        setSelectedProducts((prevProducts) => [
            ...prevProducts,
            { category_gold_id: '', weight_gold: '' },
        ]);
    }
    console.log(dataCustomer);
    
    const handleSubmit = () => {
        // const productsValid =selectedProducts && selectedProducts.every(
        //   product => product.category_gold_id !== '' && product.weight_gold !== ''
        // );


        // // Validate price
        // const isPriceValid = !isNaN(FormatAmountFloatToInt(formData.price));
        // const isInterestRateValid = !isNaN(parseFloat(formData.percent));
        // const allDetailsValid =selectedProducts && selectedProducts.every((_, index) =>
        //   selectedDetailProducts[index]?.every(
        //     detailProduct => detailProduct.category_id !== '' && detailProduct.count > 0
        //   )
        // );

        // if (!isInterestRateValid || !allDetailsValid || !productsValid || !isPriceValid || isSubmit != 'true' || selectedProducts.length === 0) {
        //   setMessage('Vui lòng kiểm tra thông tin nhập vào');
        //   return; // Prevent form submission
        // }

        //setMessage('');
        const products = selectedProducts.map((product, index) => ({
            product: {
                category_gold_id: product.category_gold_id,
                weight_gold: parseFloat(product.weight_gold),
            },
            detail_products:
                selectedDetailProducts &&
                selectedDetailProducts.length > 0 &&
                selectedDetailProducts[index].map((detailProduct) => ({
                    category_id: detailProduct.category_id,
                    count: detailProduct.count,
                })),
        }));
        const params = {
            order: {
                note: formData.note,
                product_name: formData.product_name,
                price: FormatAmountFloatToInt(formData.price),
                order_date: selectedDate == null ? formData.order_date : selectedDate,
                return_date:
                    selectedDate == null
                        ? formData.return_date
                        : ConvertNextMonth(selectedDate),
                weight_goal: parseFloat(formData.weight_goal),
                status: formData.status,
                interest_paid_count: 0,
                percent: parseFloat(formData.percent),
                customer_id: isSelectNew ? dataCustomer?.customer_id : customerID
            },
            products: products
        }
        BaseURL
            .post("create-pawn", params)
            .then((response) => {
                if (response.status === 200) {
                    setIsSuccess(true);
                }
            })
            .catch((err) => toast("Thêm đơn thất bại!"));
    };
    const handleMinusProduct = (index: number) => {
        setSelectedProducts((prevProducts) => {
            const updatedProducts = [...prevProducts];
            if (index >= 0 && index < updatedProducts.length) {
                updatedProducts.splice(index, 1);
            }
            return updatedProducts;
        });
        // Xóa mảng loại hàng liên quan khi xóa loại vàng
        setSelectedDetailProducts((prevDetailProducts) => {
            const updatedDetailProducts = [...prevDetailProducts];
            if (index >= 0 && index < updatedDetailProducts.length) {
                updatedDetailProducts.splice(index, 1);
            }
            return updatedDetailProducts;
        });
    }
    const handleGoldChange = (value: any, index: number) => {
        setSelectedProducts((prevProducts) => {
            const updatedProducts = [...prevProducts];
            if (updatedProducts[index]) {
                updatedProducts[index].category_gold_id = value;
            }
            return updatedProducts;
        });
    }
    const handleAddCategory = (productIndex: number) => {
        setSelectedDetailProducts((prevDetailProducts) => {
            const updatedDetailProducts: DetailProduct[][] = [...prevDetailProducts];
            updatedDetailProducts[productIndex] = [
                ...(updatedDetailProducts[productIndex] || []),
                { category_id: '', count: 1 },
            ];
            return updatedDetailProducts;
        });
    };
    const handleChangeDate = (date: dayjs.Dayjs | null, dateString: any) => {
        const dateS = dayjs(date);
        const nextDay = dateS.add(1, "day");
        setSelectedDate(nextDay);
    };

    const handleMinusCategoryDetail = (index: number, productIndex: number) => {
        setSelectedDetailProducts((prevDetailProducts) => {
            const updatedDetailProducts = [...prevDetailProducts];
            if (
                updatedDetailProducts[productIndex] &&
                updatedDetailProducts[productIndex][index]
            ) {
                updatedDetailProducts[productIndex] = updatedDetailProducts[productIndex].filter(
                    (item, idx) => idx !== index
                );
            }
            return updatedDetailProducts;
        });
    };
    const handleWeightGoldChange = (value: any, index: number) => {
        setSelectedProducts((prevProducts) => {
            const updatedProducts = [...prevProducts];
            if (updatedProducts[index]) {
                updatedProducts[index].weight_gold = value;
            }
            return updatedProducts;
        });
    };

    const handleCategoryChange = (value: any, index: number, productIndex: number) => {
        setSelectedDetailProducts((prevProducts) => {
            const updatedProducts = [...prevProducts];
            if (
                updatedProducts[productIndex] &&
                updatedProducts[productIndex][index]
            ) {
                updatedProducts[productIndex][index] = {
                    ...updatedProducts[productIndex][index],
                    category_id: value,
                };
            }
            return updatedProducts;
        });
    }
    const handleCountChange = (value: any, index: number, productIndex: number) => {
        setSelectedDetailProducts((prevProducts) => {
            const updatedProducts = [...prevProducts];
            if (
                updatedProducts[productIndex] &&
                updatedProducts[productIndex][index]
            ) {
                updatedProducts[productIndex][index] = {
                    ...updatedProducts[productIndex][index],
                    count: value,
                };
            }
            return updatedProducts;
        });
    };
    useEffect(() => {
        //setLoading(true);
        BaseURL
            .get("get-list-category")
            .then((response) => {
                if (response.status === 200) {
                    setDataCate(response.data);
                }
            })
            .catch((error) => {
                if (error.response) {
                    console.log("Server responded with a non-2xx status");
                    console.log(error.response.data);
                    console.log(error.response.status);
                    console.log(error.response.headers);
                } else if (error.request) {
                    console.log("No response received from the server");
                    console.log(error.request);
                } else {
                    console.log("Error setting up the request");
                    console.log(error.message);
                }
                console.log(error.config);
            });
        BaseURL
            .get("get-list-category-gold")
            .then((response) => {
                if (response.status === 200) {
                    setDataCateGold(response.data);
                }
            })
            .catch((error) => {
                if (error.response) {
                    console.log("Server responded with a non-2xx status");
                    console.log(error.response.data);
                    console.log(error.response.status);
                    console.log(error.response.headers);
                } else if (error.request) {
                    console.log("No response received from the server");
                    console.log(error.request);
                } else {
                    console.log("Error setting up the request");
                    console.log(error.message);
                }
                console.log(error.config);
            });
        BaseURL
            .get("get-list-category-percent")
            .then((response) => {
                if (response.status === 200) {
                    setDataCatePercent(response.data);
                }
            })
            .catch((error) => {
                if (error.response) {
                    console.log("Server responded with a non-2xx status");
                    console.log(error.response.data);
                    console.log(error.response.status);
                    console.log(error.response.headers);
                } else if (error.request) {
                    console.log("No response received from the server");
                    console.log(error.request);
                } else {
                    console.log("Error setting up the request");
                    console.log(error.message);
                }
                console.log(error.config);
            });
    }, []);
    const content = (
        <div className="main-panel">
            <nav className="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
                <div className="container-fluid">
                    <div className="navbar-wrapper">
                        <div className="navbar-toggle">
                            <Button className="navbar-toggler">
                                <span className="navbar-toggler-bar bar1"></span>
                                <span className="navbar-toggler-bar bar2"></span>
                                <span className="navbar-toggler-bar bar3"></span>
                            </Button>
                        </div>
                        <a className="navbar-brand" href="javascript:;">
                            THÊM ĐƠN VÀNG
                        </a>
                    </div>
                </div>
            </nav>
            <div className="content">
                <div className="row">
                    <div className="col-md-10">
                        <div className="card">
                            <div className="card-header">

                                <Form className="form-add-pawn" autoSave="off" autoComplete="off">
                                    {selectedProducts.map((product, productIndex) => (
                                        <>
                                            <div className="list-products" key={productIndex}>
                                                <Form.Item className="form-add-pawn-item" label="Loại vàng">
                                                    <div className="category-order">
                                                        <Select
                                                            placeholder="Chọn loại"
                                                            className="form-input-select"
                                                            value={product.category_gold_id}
                                                            onChange={(value) => handleGoldChange(value, productIndex)}
                                                        >
                                                            {dataCateGold.map((item) => (
                                                                <Select.Option value={item.category_id} key={item.category_id}>
                                                                    {item.category_name}
                                                                </Select.Option>
                                                            ))}
                                                        </Select>

                                                    </div>
                                                </Form.Item>
                                                <Form.Item className="form-add-pawn-item" label="Thêm loại hàng">
                                                    <Button
                                                        className="button-order-category"
                                                        onClick={() => handleAddCategory(productIndex)}
                                                    >
                                                        <i className="fa fa-plus" aria-hidden="true"></i>
                                                    </Button>
                                                </Form.Item>
                                                {selectedDetailProducts[productIndex]?.map((product, index) => (
                                                    <div className="list-products" key={index}>
                                                        <Form.Item className="form-add-pawn-item" label="Loại hàng">
                                                            <div className="category-order">
                                                                <Select
                                                                    className="form-input-select"
                                                                    onChange={(value) => handleCategoryChange(value, index, productIndex)}
                                                                    value={product.category_id}
                                                                    placeholder="Chọn loại"
                                                                >
                                                                    {dataCate.map((item) => (
                                                                        <Select.Option value={item.category_id} key={item.category_id}>
                                                                            {item.category_name}
                                                                        </Select.Option>
                                                                    ))}
                                                                </Select>
                                                                <InputNumber
                                                                    min={1}
                                                                    max={9999}
                                                                    defaultValue={1}
                                                                    className="form-input-number"
                                                                    value={product.count}
                                                                    onChange={(value) => handleCountChange(value, index, productIndex)}
                                                                />
                                                                <Button
                                                                    className="button-order-category"
                                                                    onClick={() => handleMinusCategoryDetail(index, productIndex)}
                                                                >
                                                                    <i className="fa fa-minus" aria-hidden="true"></i>
                                                                </Button>
                                                            </div>
                                                        </Form.Item>
                                                    </div>
                                                ))}
                                                <Form.Item className="form-add-pawn-item" label="Cân nặng">
                                                    <Input
                                                        className="form-input-pawn-weight"
                                                        value={product.weight_gold}
                                                        onChange={(e) => handleWeightGoldChange(e.target.value, productIndex)}
                                                        suffix="Chỉ"
                                                    />
                                                </Form.Item>

                                                <Button
                                                    className="button-remove"
                                                    onClick={() => {
                                                        handleMinusProduct(productIndex);
                                                    }}
                                                >
                                                    <i className="fa fa-times" aria-hidden="true"></i>
                                                </Button>
                                            </div>
                                        </>
                                    ))}
                                    <Form.Item className="form-add-pawn-item" label="Thêm loại vàng">
                                        <Button
                                            className="button-order-category"
                                            onClick={handleAddProduct}
                                        >
                                            <i className="fa fa-plus" aria-hidden="true"></i>
                                        </Button>

                                    </Form.Item>

                                    <Form.Item className="form-add-pawn-item" label="Giá cầm">
                                        <Input
                                            className="form-input-pawn"
                                            name="price"
                                            value={amount}
                                            onChange={handleAmountChange}
                                            suffix="VND"
                                        />
                                    </Form.Item>

                                    <Form.Item className="form-add-pawn-item" label="Lãi suất">
                                        <Select
                                            placeholder="Chọn lãi xuất"
                                            className="form-input-select"
                                            onChange={handleChangePercent}
                                        >
                                            {dataCatePercent.map((item) => (
                                                <Select.Option value={item.percent_id}>
                                                    {item.percent}
                                                </Select.Option>
                                            ))}
                                        </Select>
                                    </Form.Item>
                                    <Form.Item
                                        className="form-add-pawn-item"
                                        label="Ngày cầm"
                                        name="date"
                                    >
                                        <ConfigProvider locale={viVN}>
                                            <DatePicker
                                                className="form-input-pawn"
                                                defaultValue={dayjs(formattedDate, "YYYY-MM-DD")}
                                                onChange={handleChangeDate}
                                                locale={locale}
                                            />
                                        </ConfigProvider>
                                    </Form.Item>
                                    <Form.Item
                                        className="form-add-pawn-item"
                                        label="Ghi chú"
                                        name="note"
                                    >
                                        <TextArea
                                            name="note"
                                            onChange={handleChange}
                                        />
                                    </Form.Item>
                                    <div className="button-customer">
                                        <Button
                                            className="button-customer-list"
                                            onClick={() => {
                                                setIsOpenModal(true);
                                                setIsSelectNew(false);
                                            }}
                                        >
                                            Danh sách khách hàng
                                        </Button>
                                        <Button
                                            className="button-customer-new"
                                            onClick={() => {
                                                setNewCustomer(true);
                                                setIsSelect(false);
                                            }}
                                        >
                                            Khách hàng mới
                                        </Button>
                                    </div>
                                    {isSelect && (
                                        <CustomerInfo dataCustomer={dataCustomer} isNew={0} />
                                    )}
                                    {isSelectNew && ( 
                                        <CustomerInfo dataCustomer={dataCustomer} isNew={1} />
                                    )}
                                    <div className="message-fail-submit"></div>
                                    <div className="save-form-pawn">
                                        <div className="update ml-auto mr-auto">
                                            <button
                                                onClick={() => {
                                                    handleSubmit();
                                                }}
                                                type="submit"
                                                className="btn btn-primary btn-round"
                                            >
                                                Lưu đơn
                                            </button>
                                        </div>
                                    </div>
                                </Form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    );


    return (<>
        <TheContent content={content} active={2} />
        {newCustomer && <NewCustomerModal
            isOpen={newCustomer}
            isSelect={isSelectNew}
            setIsSelect={setIsSelectNew}
            setIsOpen={setNewCustomer}
            setDataCustomer={setDataCustomer}
        />}
        <ListCustomerModal
            setCustomerID={setCustomerID}
            isOpen={isOpenModal}
            setIsSelect={setIsSelect}
            setIsOpen={setIsOpenModal}
            setDataCustomer={setDataCustomer}
        />
    </>)
}