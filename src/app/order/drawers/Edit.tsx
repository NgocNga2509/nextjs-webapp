"use client"
import { Button, ConfigProvider, DatePicker, Drawer, Form, Input, InputNumber, Select } from "antd";
import { useEffect, useState } from "react";
import dayjs from "dayjs";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import locale from 'antd/es/date-picker/locale/vi_VN';
import viVN from "antd/lib/locale/vi_VN";
import 'dayjs/locale/vi';
import BaseURL from "@/app/api/BaseURL";
import { ConvertNextMonth, FormatAmountFloatToInt, FormatAmountIntToFloat } from "@/app/helpers/Helps";
import { IndexKind } from "typescript";
interface OrderList {
    order: Order,
    interestAmount: "",
    cate_cate_gold: ProductDetail,
}

interface Category {
    category_id: "",
    category_name: ""
}
interface CategoryPercent {
    percent_id: "",
    percent: ""
}
interface ProductDetail {
    category_gold_id: string;
    category_id: string;
    count: number;
    weight_gold: string;
}
interface Order {
    ID: "",
    CustomerID: "",
    ProductName: "",
    add_payments: "",
    payments: "",
    EstimatedPrice: "",
    Price: "",
    PriceOld: "",
    ReturnDate: "",
    Note: "",
    Miss: "",
    Status: "",
    Percent: "",
    OrderDate: "",
    Code: "",
    STT: "",
}
const DrawerEditOrder = ({
    isOpen,
    setIsOpen,
    orderID,
    resetFlag,
    setResetFlag,
}: any) => {
    const [data, setData] = useState<OrderList>();
    const [products, setProducts] = useState([]);

    const [dataCate, setDataCate] = useState<Category[]>([]);
    const [dataCateGold, setDataCateGold] = useState<Category[]>([]);
    const [dataCatePercent, setDataCatePercent] = useState<CategoryPercent[]>([]);
    const [cateCateGold, setCateCateGold] = useState<ProductDetail[]>([])

    const [selectedDate, setSelectedDate] = useState<dayjs.Dayjs | null>(null);
    const [selectedDateEnd, setSelectedDateEnd] = useState(null);
    const [form] = Form.useForm();

    const [message, setMessage] = useState({
        price: '',
        date: '',
        catgory_gold: 'Vui lòng nhập đầy đủ chi tiết sản phẩm',
    })
    useEffect(() => {
        BaseURL
            .get(`list-pawn/${orderID}`)
            .then((response) => {
                setData(response.data);
            })
            .catch((error) => {
                if (error.response) {
                    console.log("Server responded with a non-2xx status");
                    console.log(error.response.data);
                    console.log(error.response.status);
                    console.log(error.response.headers);
                } else if (error.request) {
                    console.log("No response received from the server");
                    console.log(error.request);
                } else {
                    console.log("Error setting up the request");
                    console.log(error.message);
                }
                console.log(error.config);
            });
    }, [orderID]);

    const initialFormData = {
        estimated_price: "",
        price: "",
        weight_goal: "",
        order_date: "",
        return_date: "",
        status: "",
        interest_paid_count: "",
        category_id: "",
        category_goal_id: "",
        customer_id: "",
        note: "",
        percent: "",
        cateCateGold: [] as ProductDetail[] | undefined,
    };

    const [formData, setFormData] = useState(initialFormData);
    useEffect(() => {

        setFormData({
            ...formData,
            estimated_price: formatNumber(data?.order.EstimatedPrice) || "",
            price: formatNumber(data?.order.Price) || "",
            // order_date: dayjs(data.order_date).format("YYYY-MM-DD"),
            // return_date: dayjs(data.return_date).format("YYYY-MM-DD"),
            order_date: data?.order.OrderDate || "",
            return_date: data?.order.ReturnDate || "",
            status: data?.order.Status || "",
            customer_id: data?.order.CustomerID || "",
            percent: data?.order.Percent || "",
            note: data?.order.Note || "",
            cateCateGold: Array.isArray(data?.cate_cate_gold) ? data?.cate_cate_gold : [],
        })
    }, [data])
    useEffect(() => {
        if (formData.cateCateGold?.length != 0) {
            setMessage({ ...message, catgory_gold: '' });
        } else {
            setMessage({ ...message, catgory_gold: 'Vui lòng nhập đầy đủ chi tiết sản phẩm' });
            return
        }
        formData.cateCateGold?.map((product, productIndex) => {
            if (product.category_gold_id != '') {
                setMessage({ ...message, catgory_gold: '' });
            } else {
                setMessage({ ...message, catgory_gold: 'Vui lòng nhập đầy đủ chi tiết sản phẩm' });
                return
            }
            if (product.category_id != '') {
                setMessage({ ...message, catgory_gold: '' });
            } else {
                setMessage({ ...message, catgory_gold: 'Vui lòng nhập đầy đủ chi tiết sản phẩm' });
                return
            }
            if (product.count != 0) {
                setMessage({ ...message, catgory_gold: '' });
            } else {
                setMessage({ ...message, catgory_gold: 'Vui lòng nhập đầy đủ chi tiết sản phẩm' });
                return
            }
            if (product.weight_gold != '') {
                setMessage({ ...message, catgory_gold: '' });
            } else {
                setMessage({ ...message, catgory_gold: 'Vui lòng nhập đầy đủ chi tiết sản phẩm' });
                return
            }

        })
    }, [formData.cateCateGold])
    const onClose = () => {
        //setResetFlag(resetFlag + 1)
        setIsOpen(false);
    };

    const formatNumber = (number: any) => {
        if (typeof number === "number") {
            const formattedNumber = number
                .toString()
                .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return formattedNumber;
        } else {
            return 0;
        }
    };


    const handleSubmit = () => {
        const catecategold = formData.cateCateGold?.map((detailProduct: any) => ({
            category_gold_id: detailProduct.category_gold_id,
            weight_gold: parseFloat(detailProduct.weight_gold),
            category_id: detailProduct.category_id,
            count: detailProduct.count,
        }))

        const params = {
            order: {
                id: orderID,
                price: FormatAmountFloatToInt(formData.price),
                order_date: selectedDate == null ? formData.order_date : selectedDate,
                return_date:
                    selectedDate == null
                        ? formData.return_date
                        : ConvertNextMonth(selectedDate),
                status: formData.status,
                customer_id: formData?.customer_id,
                interest_paid_count: 0,
                percent: parseFloat(formData.percent),
                note: formData.note,
            },
            //products: products
            cate_cate_gold: catecategold,
        }
        BaseURL
            .put("update-pawn", params)
            .then((response) => {
                if (response.status === 200) {
                    //setIsSuccess(true);
                    setIsOpen(false)
                    setResetFlag(resetFlag + 1)
                }
            })
            .catch((err) => toast("Sửa đơn thất bại!"));
    }

    useEffect(() => {
        BaseURL
            .get("get-list-category")
            .then((response) => {
                if (response.status === 200) {
                    setDataCate(response.data);
                }
            })
            .catch((error) => {
                if (error.response) {
                    console.log("Server responded with a non-2xx status");
                    console.log(error.response.data);
                    console.log(error.response.status);
                    console.log(error.response.headers);
                } else if (error.request) {
                    console.log("No response received from the server");
                    console.log(error.request);
                } else {
                    console.log("Error setting up the request");
                    console.log(error.message);
                }
                console.log(error.config);
            });
        BaseURL
            .get("get-list-category-gold")
            .then((response) => {
                if (response.status === 200) {
                    setDataCateGold(response.data);
                }
            })
            .catch((error) => {
                if (error.response) {
                    console.log("Server responded with a non-2xx status");
                    console.log(error.response.data);
                    console.log(error.response.status);
                    console.log(error.response.headers);
                } else if (error.request) {
                    console.log("No response received from the server");
                    console.log(error.request);
                } else {
                    console.log("Error setting up the request");
                    console.log(error.message);
                }
                console.log(error.config);
            });
        BaseURL
            .get("get-list-category-percent")
            .then((response) => {
                if (response.status === 200) {
                    setDataCatePercent(response.data);
                }
            })
            .catch((error) => {
                if (error.response) {
                    console.log("Server responded with a non-2xx status");
                    console.log(error.response.data);
                    console.log(error.response.status);
                    console.log(error.response.headers);
                } else if (error.request) {
                    console.log("No response received from the server");
                    console.log(error.request);
                } else {
                    console.log("Error setting up the request");
                    console.log(error.message);
                }
                console.log(error.config);
            });
    }, []);

    const handleChangeDate = (date: any, dateString: any) => {
        if (date != null) {
            const dateS = dayjs(date);
            const nextDay = dateS.add(1, "day");
            const oneMonthLater = dateS.add(1, "month");

            setSelectedDate(nextDay);
            setFormData((prevData) => ({
                ...prevData,
                order_date: dateS.format("YYYY-MM-DD"), // Format the date as a string
                return_date: oneMonthLater.format("YYYY-MM-DD"), // Set return_date one month later
            }));
        };
    };

    const handleAddProduct = () => {
        const newProduct: ProductDetail = {
            category_gold_id: '',
            category_id: '',
            weight_gold: '',
            count: 1,
        };

        setFormData((prevData) => ({
            ...prevData,
            cateCateGold: [...(prevData.cateCateGold || []), newProduct],
        }));
    }
    const handleAmountChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const value = event.target.value;
        const formattedValue = FormatAmountIntToFloat(value);

        setFormData((prevData) => ({
            ...prevData,
            price: formattedValue || '',
        }));

        if (value == "") {
            setMessage({ ...message, price: 'Vui lòng nhập giá cầm!' })
        } else {
            setMessage({ ...message, price: '' })
        }
    };
    const handleMinusProductDetail = (indexToRemove: any) => {
        setFormData((prevData) => {
            const updatedCateCateGold = prevData.cateCateGold?.filter((item, index) => index !== indexToRemove);
            return {
                ...prevData,
                cateCateGold: updatedCateCateGold,
            };
        });
    };
    const handleChangeProduct = (newValue: any, index: any, fieldName: keyof ProductDetail) => {

        setFormData((prevData) => {
            const updatedCateCateGold = [...(prevData.cateCateGold || [])];
            const productToUpdate = updatedCateCateGold[index];

            (productToUpdate as any)[fieldName] = newValue;

            return {
                ...prevData,
                cateCateGold: updatedCateCateGold,
            };
        });
    };
    const handleSelect = (e: any, fieldName: any) => {
        setFormData({ ...formData, [fieldName]: e })
    }

    const handleNoteChange = (e: any) => {
        setFormData({ ...formData, note: e.target.value })
    }
    return (
        <>
            <Drawer
                zIndex={1030}
                className="drawer-detail"
                title={
                    <div className="drawer-detail-title">
                        SỬA HỒ SƠ<div className="drawer-detail-id">(Mã đơn:{data?.order.ID})</div>
                    </div>
                }
                placement="right"
                onClose={onClose}
                open={isOpen}
                width={700}
            >
                <Form className="form-edit-order" autoSave="off" autoComplete="off" form={form}>
                    <Form.Item className="form-edit-order-label" label="Mã khách hàng">
                        <Input
                            className="form-edit-order-input"
                            value={formData.customer_id}
                            disabled
                        />
                    </Form.Item>
                    <Form.Item className="form-edit-order-label" label={<span className="required-label">Sản phẩm</span>}>
                        <Button
                            className="button-order-category"
                            onClick={() => handleAddProduct()}
                        >
                            <i className="fa fa-plus" aria-hidden="true"></i>
                        </Button>
                        <div className="c-red">{message.catgory_gold}</div>
                    </Form.Item>
                    <div className="products-detail">
                        {formData.cateCateGold?.map((item: any, index: any) => (
                            <li>
                                <div className="d-flex" key={index}>
                                    <Select
                                        className="form-edit-order-input"
                                        value={item.category_gold_id}
                                        onChange={(value) => handleChangeProduct(value, index, 'category_gold_id')}
                                    >
                                        {dataCateGold.map((item) => (
                                            <Select.Option value={item.category_id} key={item.category_id}>
                                                {item.category_name}
                                            </Select.Option>
                                        ))}
                                    </Select>
                                    <Select
                                        className="form-edit-order-input"
                                        value={item.category_id}
                                        onChange={(value) => handleChangeProduct(value, index, 'category_id')}
                                    >
                                        {dataCate.map((item) => (
                                            <Select.Option value={item.category_id} key={item.category_id}>
                                                {item.category_name}
                                            </Select.Option>
                                        ))}
                                    </Select>
                                    <Input
                                        className="form-edit-order-input-20"
                                        value={item.weight_gold}
                                        suffix="Chỉ"
                                        onChange={(event) => handleChangeProduct(event.target.value, index, 'weight_gold')}
                                    />
                                    <InputNumber
                                        min={1}
                                        max={9999}
                                        className="form-input-number"
                                        onChange={(value) => handleChangeProduct(value, index, 'count')}
                                        value={item.count}
                                        width={100}
                                    />
                                    <Button
                                        className="button-order-category"
                                        onClick={() => handleMinusProductDetail(index)}
                                    >
                                        <i className="fa fa-minus" aria-hidden="true"></i>
                                    </Button>
                                </div>
                            </li>
                        )
                        )}
                    </div>
                    <Form.Item className="form-edit-order-label" label={<span className="required-label">Trạng thái</span>}>
                        <Select
                            className="form-edit-order-select"
                            value={formData.status}
                            onChange={(e) => handleSelect(e, 'status')}
                        //name="status"
                        >
                            <Select.Option value="0">Còn hiệu lực</Select.Option>
                            <Select.Option value="1">Đã xong</Select.Option>
                            <Select.Option value="2">Đã hóa giá</Select.Option>
                        </Select>
                    </Form.Item>
                    {/* <Form.Item className="form-edit-order-label" label="Ghi chú">
                        <Input
                            className="form-edit-order-input"
                            name="note"
                            value={formData.note}
                            onChange={handleNoteChange}
                            suffix="VND"
                        />
                    </Form.Item> */}
                    <Form.Item className={message.price ? "b-red form-edit-order-label" : "form-edit-order-label"}
                        label={<span className="required-label">Giá cầm</span>}
                    >
                        <Input
                            className="form-edit-order-input"
                            name="price"
                            value={formData.price}
                            onChange={handleAmountChange}
                            suffix="VND"
                        />
                        <div className="c-red">{message.price}</div>
                    </Form.Item>
                    <Form.Item className="form-edit-order-label" label={<span className="required-label">Lãi suất</span>}>
                        <Select
                            className="form-edit-order-select"
                            onChange={(e) => handleSelect(e, 'percent')}
                            value={formData.percent}
                        >
                            {dataCatePercent.map((item) => (
                                <Select.Option value={item.percent_id}>
                                    {item.percent}
                                </Select.Option>
                            ))}
                        </Select>
                    </Form.Item>
                    <Form.Item className="form-edit-order-label" label={<span className="required-label">Ngày cầm</span>}>
                        <ConfigProvider locale={viVN}>
                            <DatePicker
                                className="form-edit-order-input"
                                value={dayjs(formData.order_date, "YYYY-MM-DD")}
                                name="order_date"
                                onChange={handleChangeDate}
                                locale={locale}
                            />
                        </ConfigProvider>
                    </Form.Item>
                    <Form.Item className="form-edit-order-label" label="Ngày hết hạn">
                        <ConfigProvider locale={viVN}>
                            <DatePicker
                                className="form-edit-order-input"
                                value={dayjs(formData.return_date, "YYYY-MM-DD")}
                                name="return_date"
                                //onChange={handleChangeDateEnd}
                                disabled
                                locale={locale}
                            />
                        </ConfigProvider>
                    </Form.Item>
                    <div className="form-edit-order-save">
                        {/* <Button className="btn-cancel" onClick={() => onSave()}>
                            Huỷ
                        </Button> */}
                        <Button className="btn-ok"
                            onClick={() => {
                                //setIsSubmit(true)
                                form
                                    .validateFields()
                                    .then(() => {
                                        message.catgory_gold == "" &&
                                            message.price == "" &&
                                            handleSubmit();
                                    })
                                    .catch((errorInfo) => {
                                        console.log('Validation failed:', errorInfo);
                                    });
                                // onClick={() => handleSubmit()}>

                            }}>
                            Lưu thay đổi
                        </Button>
                    </div>
                </Form>
            </Drawer >
            <ToastContainer autoClose={1000} theme="dark" position="top-center" />
        </>
    );
};
export default DrawerEditOrder;

