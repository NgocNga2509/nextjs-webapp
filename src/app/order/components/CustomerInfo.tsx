import { FormatDate } from "@/app/helpers/Helps"

export default function CustomerInfo({dataCustomer, isNew}: any) {
    const data = {
        CustomerName:isNew == 1 ? dataCustomer?.customer_name : dataCustomer?.CustomerName,
        BirthDate:isNew == 1 ? dataCustomer?.birth_date : dataCustomer?.BirthDate,
        Gender:isNew == 1 ? dataCustomer?.gender : dataCustomer?.Gender,
        CCCD:isNew == 1 ? dataCustomer?.cccd : dataCustomer?.CCCD,
        Phone:isNew == 1 ? dataCustomer?.phone : dataCustomer?.Phone,
    }
    return (
        <>
            <div className="customer-old">
                <div className="info-customer">
                    <div className="info-customer-label">
                        Tên khách hàng:
                    </div>
                    <div>{data.CustomerName}</div>
                </div>
                <div className="info-customer">
                    <div className="info-customer-label">
                        Ngày tháng năm sinh:
                    </div>
                    <div>
                    {data.BirthDate && <FormatDate date={data.BirthDate} />}
                    </div>
                </div>
                <div className="info-customer">
                    <div className="info-customer-label">Giới tính:</div>
                    <div>{data.Gender == "" ? "" : data.Gender == "0" ? "Nữ" : "Nam"}</div>
                </div>
                <div className="info-customer">
                    <div className="info-customer-label">CCCD:</div>
                    <div>{data.CCCD}</div>
                </div>
                <div className="info-customer">
                    <div className="info-customer-label">SĐT:</div>
                    <div>{data.Phone}</div>
                </div>
            </div>
        </>
    )
}