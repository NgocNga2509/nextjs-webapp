"use client"

import { useEffect, useState, useRef } from "react";
import TheContent from "../../helpers/TheContent";
import Lottie from 'react-lottie-player'
import {
    Button,
    ConfigProvider,
    DatePicker,
    Input,
    Pagination,
    Popover,
    Select,
    Table,
} from "antd";
import { InfoCircleOutlined } from "@ant-design/icons";
import locale from "antd/es/date-picker/locale/vi_VN";
import viVN from "antd/lib/locale/vi_VN";
import BaseURL from "@/app/api/BaseURL";
import moment from "moment";
import { FormatCurrency, FormatDate } from "@/app/helpers/Helps";
import DrawerDetail from "../drawers/Detail";
import DrawerEditOrder from "../drawers/Edit";
import UpdateNote from "../modals/UpdateNote";
import { showDeleteConfirm } from "@/app/helpers/Modals";
interface OrderList {
    order: Order,
    products: Product,
    add_payments: AddPayments,
    payments: Payments,
    interestAmount: "",
    cate_cate_gold: ProductDetail,
}
interface Product {
    CategoryID: "",
}
interface Payments {
    Payments: "",
}
interface AddPayments {
    AddPayment: "",
}
interface ProductDetail {
    category_gold_id: string;
    category_id: string;
    count: number;
    weight_gold: Float32Array;
}
interface Order {
    ID: "",
    CustomerID: "",
    ProductName: "",
    add_payments: "",
    payments: "",
    EstimatedPrice: "",
    Price: "",
    PriceOld: "",
    ReturnDate: "",
    Note: "",
    Miss: "",
    WeightGoal: "",
    Status: "",
    Percent: "",
    OrderDate: "",
    Code: "",
    STT: "",
}
interface CategoryList {
    category_id: string;
    category_name: string;
}

interface CategoryGoldList {
    category_id: string;
    category_name: string;
}
interface CategoryPercentList {
    percent_id: string;
    percent: string;
}
interface CustomerList {
    address: string;
    address_cccd: string;
    birth_date: string;
    cccd: string;
    created_at: string;
    customer_id: string;
    customer_name: string;
    day_cccd: string;
    gender: string;
    phone: string;
    updated_at: string;
}

type PopoverContent = { [key: string]: string };

const ListOrderGoal = () => {
    const [loading, setLoading] = useState(true);
    const [loadingSearch, setLoadingSearch] = useState(false);
    const [active, setActive] = useState(4);
    const [subMenu, setSubMenu] = useState(true);
    const [orderID, setOrderID] = useState("");
    const [data, setData] = useState<OrderList[]>([]);
    const [popoverContent, setPopoverContent] = useState<PopoverContent>({});
    const [modalPay, setModalPay] = useState(false);
    const [resetFlag, setResetFlag] = useState(1);
    const [openDrawer, setOpenDrawer] = useState(false);
    const [openDrawerEdit, setOpenDrawerEdit] = useState(false);
    const [openModalPay, setOpenModalPay] = useState(false);
    const [openModalAddPayment, setOpenModalAddPayment] = useState(false);
    const [expandSidebar, setExpandSidebar] = useState(false);
    const [openModalDelete, setOpenModalDelete] = useState(false);
    const [isUpdateNote, setIsUpdateNote] = useState(false);
    const [percent, setPercent] = useState<CategoryPercentList[]>([]);
    const [search, setSearch] = useState("");
    const [filteredData, setFilteredData] = useState(0);
    const [customerData, setCustomerData] = useState<CustomerList[]>([]); // Add a state variable to store the customer name
    const [code, setCode] = useState("");
    const [isSearch, setIsSearch] = useState(false);
    const [datePickerKey, setDatePickerKey] = useState(0); // Initialize with 0

    const [categoryList, setCategoryList] = useState<CategoryList[]>([])
    const [categoryGoldList, setCategoryGoldList] = useState<CategoryGoldList[]>([])

    const convertCustomer = (id: any) => {
        let name = "";
        customerData.map((item) => {
            if (item.customer_id == id) {
                name = item.customer_name;
            }
        });
        return name;
    };
    const fetchData = async (id: any) => {
        try {
            const response = await BaseURL.get(`list-customer/${id}`);
            const customerData = response.data;

            const content = (
                <div className="content-customer">
                    <ul>
                        <li>
                            <b>Mã:</b> <div>{customerData.customer_id}</div>
                        </li>
                        <li>
                            <b>CCCD:</b> <div>{customerData.cccd}</div>
                        </li>
                        <li>
                            <b>SĐT:</b> <div>{customerData.phone}</div>
                        </li>
                        <li>
                            <b>Địa chỉ:</b>
                            <div>{customerData.address}</div>{" "}
                        </li>
                    </ul>
                </div>
            );

            setPopoverContent({ ...popoverContent, [id]: content });
        } catch (error) {
            console.error("Error fetching customer data:", error);
        }
    };
    const isOverdue = (returnDate: Date): number => {
        const currentDate = new Date();
        const returnDateObj = new Date(returnDate);
        const currentTimeStamp = currentDate.getTime();
        const returnTimeStamp = returnDateObj.getTime();

        const diffInDays = Math.floor((currentTimeStamp - returnTimeStamp) / (1000 * 60 * 60 * 24));

        // Create a new Date object instead of modifying the original one
        const returnDateObjMinusOneMonth = new Date(returnDate);
        returnDateObjMinusOneMonth.setMonth(returnDateObjMinusOneMonth.getMonth() - 1);

        const returnTimeStampMinusOneMonth = returnDateObjMinusOneMonth.getTime();
        const diffInDays2 = Math.floor((currentTimeStamp - returnTimeStampMinusOneMonth) / (1000 * 60 * 60 * 24));

        if (diffInDays > 0 && diffInDays2 >= 60) {
            return 2;
        } else if (diffInDays > 0) {
            return 1;
        } else {
            return 0;
        }
    };

    useEffect(() => {
        BaseURL.get("get-list-category").then((response) => {
            if (response.status == 200) {
                setCategoryList(response.data)
            }
        }).catch((err) => console.log(err))
        BaseURL.get("get-list-category-gold").then((response) => {
            if (response.status == 200) {
                setCategoryGoldList(response.data)
            }
        }).catch((err) => console.log(err))
        BaseURL.get("get-list-category-percent").then((response) => {
            if (response.status == 200) {
                setPercent(response.data)
            }
        }).catch((err) => console.log(err))
        BaseURL.get("list-customer").then((response) => {
            if (response.status == 200) {
                setCustomerData(response.data)
            }
        }).catch((err) => console.log(err))
    }, [])


    const convertCategoryFromIDtoName = (id: any) => {
        let cateName = "";
        categoryList.map((item) => {
            if (item.category_id == id) {
                cateName = item.category_name;
            }
        });
        return cateName;
    }

    const convertCategoryGoldFromIDtoName = (id: any) => {
        let cateName = "";
        categoryGoldList.map((item) => {
            if (item.category_id == id) {
                cateName = item.category_name;
            }
        });
        return cateName;
    }

    const handleSelectAction = (e: any, id: any) => {
        if (e === 0) {
            setOrderID(id);
            setOpenDrawer(true);
        } else if (e === 1) {
            setOrderID(id);
            setOpenDrawerEdit(true);
        } else if (e === 2) {
            setOrderID(id);
            showDeleteConfirm(() => handleDelete(id));
        }
    };

    const convertPercent = (cate: any) => {
        let name = "";
        percent.map((item) => {
            if (item.percent_id == cate) {
                name = item.percent;
            }
        });
        return name;
    };

    const convertStatus = (status: any) => {
        let stt = '';
        let color = '';
        let fontWeight = 'bold';

        if (status === "0") {
            stt = "Còn hiệu lực";
            color = "green";
        } else if (status === "1") {
            stt = "Đã chuộc tài sản";
            color = "#86518a";
        } else {
            stt = "Hồ sơ hóa giá";
            color = "red";
        }

        return (
            <span style={{ color, fontWeight }}>
                {stt}
            </span>
        );
    };

    const handleDelete = (id: any) => {
        const param = {
            id: id,
            status: 0,
        }
        const config = {
            data: param,
        };
        BaseURL.delete(`delete-pawn`, config).then((response) => {
            if (response.status === 200) {
                setResetFlag(resetFlag + 1);
                setOpenModalDelete(false);
            }
        });
    };

    const renderReturnDate = (interests: any, record: any) => {
        return (
            <div>
                {record.Status == "0" && (
                    <ul className="ul-payments-info">
                        {record.Payments && record.Payments != null ? (
                            <li>
                                <div className="li-date">
                                    <FormatDate
                                        date={
                                            record.Payments[record.Payments.length - 1]
                                                .NextPaymentDate
                                        }
                                    />
                                    {isOverdue(
                                        record.Payments[record.Payments.length - 1].NextPaymentDate
                                    ) === 2 ? (
                                        <div>Hóa giá</div>
                                    ) : isOverdue(
                                        record.Payments[record.Payments.length - 1]
                                            .NextPaymentDate
                                    ) ? (
                                        <div>Trễ hạn</div>
                                    ) : (
                                        <div></div>
                                    )}
                                </div>
                            </li>
                        ) : (
                            <li>
                                <div className="li-date">
                                    <FormatDate date={record.ReturnDate} />
                                    {isOverdue(record.ReturnDate) === 2 ? (
                                        <div>Hóa giá</div>
                                    ) : isOverdue(record.ReturnDate) === 1 ? (
                                        <div>Trễ hạn</div>
                                    ) : (
                                        <div></div>
                                    )}
                                </div>
                            </li>
                        )}
                    </ul>
                )}
            </div>
        );
    };
    const renderNote = (note: any, record: any) => {
        return (
            <div className="note">
                <div>
                    {note != null && (
                        <div className="note-content">
                            <div>{record.Miss == 1 && "Khách mất giấy"}</div>
                            {note}
                        </div>
                    )}
                    <div className="note-update">
                        <Button
                            onClick={() => {
                                setOrderID(record.ID);
                                setIsUpdateNote(true);
                            }}
                        >
                            <i className="fa fa-pencil-square-o" aria-hidden="true"></i>
                        </Button>
                    </div>
                </div>
            </div>
        );
    };
    const columns = [
        {
            title: "Mã đơn",
            dataIndex: "ID",
            key: "ID",
            width: 200,
            render: (id: any, record: any) => (
                <div>
                    {record.ID}
                </div>
            ),
        },
        {
            title: "Tên khách hàng",
            dataIndex: "CustomerID",
            key: "CustomerID",
            render: (id: any) => {
                return (
                    <div className="info-id-customer">
                        <div className="info-id-customer-id">{convertCustomer(id)}</div>
                        <div className="info-id-customer-icon">
                            <Popover
                                content={popoverContent[id]}
                                trigger="hover"
                                title="Thông tin chi tiết khách hàng:"
                            >
                                <InfoCircleOutlined onMouseEnter={() => fetchData(id)} />
                            </Popover>
                        </div>
                    </div>
                );
            },
        },
        {
            title: "Thông tin sản phẩm",
            dataIndex: "Details",
            key: "Details",
            width: 400,
            // responsive: ["xl", "lg", "md", "sm"],
            render: (e: any) =>
                e &&
                e.map((item: any, index: any) => (
                    <div key={index}>
                        {item.count} {convertCategoryFromIDtoName(item.category_id)} - {convertCategoryGoldFromIDtoName(item.category_gold_id)} - {item.weight_gold} chỉ
                    </div>
                )),
        },
        {
            title: "Giá cầm",
            dataIndex: "Price",
            key: "Price",
            width: 250,
            render: (amount: any, record: any) => (
                <div className="add-payment">
                    <div className="add-payment-new-amount">
                        <FormatCurrency amount={record.Price} />
                    </div>
                    {record.AddPayment != null && (
                        <div className="add-payment-old-amount">
                            Tiền gốc: <FormatCurrency amount={record.PriceOld} />
                        </div>
                    )}
                    {record.AddPayment &&
                        record.AddPayment.map((item: any) => (
                            <div>
                                Cầm thêm <FormatCurrency amount={item.Amount} /> ngày{" "}
                                <FormatDate date={item.DateAddPayment} />
                            </div>
                        ))}
                </div>
            ),
        },
        {
            title: "Lãi suất",
            dataIndex: "Percent",
            key: "Percent",
            render: (percent: any) => convertPercent(percent),
        },
        {
            title: "Trạng thái hồ sơ",
            dataIndex: "Status",
            key: "Status",
            width: 250,
            render: (stt: any) => (
                <span style={{ width: "200px" }}>{convertStatus(stt)}</span>
            ),
        },
        {
            title: "Ngày cầm",
            dataIndex: "OrderDate",
            key: "OrderDate",
            render: (date: any) => moment(date).format("DD/MM/YYYY"),
        },
        {
            title: "Thông tin đóng lãi",
            dataIndex: "Payments",
            key: "Payments",
            //   render: (item, record) => (
            //     <div>
            //       {record.Status == "0" && (
            //         <ul className="ul-payments-info">
            //           {record.Payments && record.Payments != null ? (
            //             record.Payments.map((interest) => (
            //               <li key={interest.ID}>
            //                 <div className="li-date">
            //                   <FormatDate date={interest.PaymentDate} />
            //                 </div>
            //                 <div className="li-amount">
            //                   <FormatCurrency amount={interest.Amount} />{" "}
            //                 </div>
            //               </li>
            //             ))
            //           ) : (
            //             <li>Chưa đóng lãi</li>
            //           )}
            //         </ul>
            //       )}
            //     </div>
            //   ),
        },
        {
            title: "Ngày tới hạn",
            dataIndex: "Payments",
            key: "Payments",
            render: (item: any, record: any) => renderReturnDate(item, record),
        },
        {
            title: "Ghi chú",
            dataIndex: "Note",
            key: "Note",
            width: 200,
            render: (note: any, record: any) => renderNote(note, record),
        },
        {
            title: "",
            dataIndex: "ID",
            key: "ID",
            render: (id: any, record: any) => (
                <div className="button-action">
                    {record.Status != "1" && (
                        <>
                            <Button
                                className="button-action-detail"
                                onClick={() => {
                                    setOrderID(id);
                                    setModalPay(true);
                                }}
                            >
                                Đóng lãi
                            </Button>

                            <Button
                                className="button-action-payment"
                                onClick={() => {
                                    setOrderID(id);
                                    setOpenModalPay(true);
                                }}
                            >
                                Thanh toán
                            </Button>
                            <Button
                                className="button-action-payment"
                                onClick={() => {
                                    setOrderID(id);
                                    setOpenModalAddPayment(true);
                                }}
                            >
                                Cầm thêm
                            </Button>
                        </>
                    )}
                    {record.Status == 0 ?
                        (
                            <Select
                                defaultValue="Thao tác"
                                className="select-search-action"
                                onChange={(e) => handleSelectAction(e, id)}
                            >
                                <Select.Option value={0}>Chi tiết</Select.Option>
                                <Select.Option value={1}>Sửa</Select.Option>
                                <Select.Option value={2}>Xóa</Select.Option>
                            </Select>
                        ) :
                        <Button
                            className="button-action-payment"
                            onClick={() => {
                                handleSelectAction(0, id)
                            }}
                        >
                            Chi tiết
                        </Button>
                    }
                </div>
            ),
        },
    ];

    const dataSource =
        data &&
        data.map((item, index) => ({
            key: index,
            ID: item.order.ID,
            CustomerID: item.order.CustomerID,
            ProductName: item.order.ProductName,
            CategoryID: item.products && item.products,
            AddPayment: item.add_payments,
            Payments: item.payments,
            EstimatedPrice: item.order.EstimatedPrice,
            Price: item.order.Price,
            PriceOld: item.order.PriceOld,
            ReturnDate: item.order.ReturnDate,
            Note: item.order.Note,
            Miss: item.order.Miss,
            WeightGoal: item.order.WeightGoal,
            Status: item.order.Status,
            Percent: item.order.Percent,
            OrderDate: item.order.OrderDate,
            Code: item.order.Code,
            STT: item.order.STT,
            interestAmount: item.interestAmount,
            Details: item.cate_cate_gold && item.cate_cate_gold,
        }));
    console.log(data);

    const [currentPage, setCurrentPage] = useState(1); // Trang hiện tại
    const pageSize = 8; // Kích thước trang
    // Số lượng tổng cộng các mục dữ liệu
    const totalItems = dataSource && dataSource.length;
    const handlePageChange = (page: any) => {
        setCurrentPage(page);
    };

    const handleChangeFilter = (value: any) => {
        setFilteredData(value);
    };

    const filterMiss = (data: any) => {
        if (filteredData === 1) {
            return data.filter((item: any) => item && item.Miss === 1);
        } else {
            return data;
        }
    };

    const currentPageData =
        dataSource &&
        dataSource.slice((currentPage - 1) * pageSize, currentPage * pageSize);

    const fetchDataSearch = async () => {
        setLoadingSearch(true)
        try {
            const response = await BaseURL.get(`list-pawn/${search}`);
            const dataFromApi = response.data;
            setData([dataFromApi]);
            setTimeout(() => {
                setLoadingSearch(false)
            }, 500)
        } catch (error) {
            setData([])
            setTimeout(() => {
                setLoadingSearch(false)
            }, 1000)
            console.error("Error fetching data:", error);
        }
    };
    const fetchDataPage = async () => {
        setLoading(true)
        BaseURL.get("list-pawn").then((response) => {
            if (response.status == 200) {
                setData(response.data)
                setLoading(false)
            }
        }).catch((err) => console.log(err))
    };

    const getRowClassName = (record: any, index: any) => {
        return index % 2 === 0 ? "even-row" : "odd-row";
    };

    useEffect(() => {
        isSearch == false && filteredData == 0 ? fetchDataPage() : fetchDataSearch();
        setCurrentPage(1);
    }, [resetFlag, isSearch, filteredData]);

    const handleChangeMonth = (dateString: any) => {
        if (dateString == null) {
            setCode("");
        } else {
            const date = new Date(dateString);
            const year = date.getFullYear().toString().slice(-2); // Lấy hai chữ số cuối của năm
            const month = (date.getMonth() + 1).toString().padStart(2, "0"); // Đảm bảo tháng luôn có hai chữ số
            const code = month + year;
            setCode(code);
        }
    };

    const handleReset = () => {
        setSearch("");
        setCode("");
        setFilteredData(0);
        setIsSearch(false);
        setDatePickerKey(datePickerKey + 1);
        setResetFlag(resetFlag + 1);
    };

    const content = (
        <>

            <div className="main-panel">
                <nav className="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
                    <div className="container-fluid">
                        <div className="navbar-wrapper">
                            <div className="navbar-toggle">
                                <Button
                                    // type="button"
                                    className="navbar-toggler"
                                    onClick={() => setExpandSidebar(true)}
                                >
                                    <span className="navbar-toggler-bar bar1"></span>
                                    <span className="navbar-toggler-bar bar2"></span>
                                    <span className="navbar-toggler-bar bar3"></span>
                                </Button>
                            </div>
                            <a className="navbar-brand" href="javascript:;">
                                QUẢN LÝ ĐƠN
                            </a>
                        </div>
                    </div>
                </nav>

                <div className="content">
                    {loading ? (
                        <div className="loading">
                            <Lottie
                                loop
                                path="/img/AnimationLoading.json"
                                play
                                style={{ width: 300, height: 300 }}
                            />
                        </div>
                    ) : (
                        <div className="row">
                            <div className="col-md-12">
                                <div className="card">
                                    <div className="card-header">
                                        <div className="d-flex-column search-customer">
                                            <div className="d-flex">
                                                <div className="d-flex">
                                                    <Input
                                                        type="text"
                                                        className="form-control"
                                                        placeholder="Tìm kiếm..."
                                                        value={search}
                                                        onInput={(e: any) => setSearch(e.target.value)}
                                                        onKeyUp={(e) => {
                                                            if (e.key === "Enter") {
                                                                setIsSearch(true);
                                                                fetchDataSearch();
                                                            }
                                                        }}
                                                    />
                                                    <div className="input-group-append cursor-pointer">
                                                        <div
                                                            className="input-group-text"
                                                            onClick={() => {
                                                                setIsSearch(true);
                                                                fetchDataSearch();
                                                            }}
                                                        >
                                                            <i className="nc-icon nc-zoom-split"></i>
                                                        </div>
                                                    </div>
                                                    <div className="input-group-append reset">
                                                        <div
                                                            className="input-group-text"
                                                            onClick={() => {
                                                                handleReset();
                                                            }}
                                                        >
                                                            <i className="fa fa-refresh" aria-hidden="true"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            {/* <Select
                                                className="select-search"
                                                defaultValue={0}
                                                onChange={handleChangeFilter}
                                                value={filteredData}
                                            >
                                                <Select.Option value={0}>Tất cả</Select.Option>
                                                <Select.Option value={1}>Khách mất giấy</Select.Option>
                                            </Select> */}
                                        </div>
                                    </div>
                                    <div className="card-body">
                                        {loadingSearch ? (
                                            <div className="loading">
                                                <Lottie
                                                    loop
                                                    path="/img/AnimationLoadingSearch.json"
                                                    play
                                                    style={{ width: 400, height: 400 }}
                                                />
                                            </div>
                                        ) : (
                                            <div className="table-responsive">
                                                <Table
                                                    columns={columns}
                                                    dataSource={currentPageData}
                                                    pagination={false}
                                                    rowClassName={getRowClassName}
                                                />
                                                <Pagination
                                                    showSizeChanger={false}
                                                    className="pagination-category"
                                                    current={currentPage}
                                                    pageSize={pageSize}
                                                    total={totalItems}
                                                    onChange={handlePageChange}
                                                />
                                            </div>
                                        )}
                                    </div>
                                </div>
                            </div>
                        </div>
                    )}
                </div>
            </div>
        </>
    );
    return (
        <>
            <TheContent
                content={content}
                active={3}
            />
            {openDrawer &&
                <DrawerDetail
                    open={openDrawer}
                    setOpen={setOpenDrawer}
                    orderID={orderID}
                />
            }
            {openDrawerEdit &&
                <DrawerEditOrder
                    isOpen={openDrawerEdit}
                    setIsOpen={setOpenDrawerEdit}
                    orderID={orderID}
                    resetFlag={resetFlag}
                    setResetFlag={setResetFlag}
                />
            }
            {isUpdateNote &&
                <UpdateNote
                    isOpen={isUpdateNote}
                    setIsOpen={setIsUpdateNote}
                    id={orderID}
                    resetFlag={resetFlag}
                    setResetFlag={setResetFlag}
                />}
        </>
    );
};
export default ListOrderGoal;
