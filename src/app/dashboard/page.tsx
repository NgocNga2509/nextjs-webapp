"use client"

import Lottie from "react-lottie-player";
import TheContent from "../helpers/TheContent"
import { useEffect, useState } from "react"
export default function Dashboard() {
  const [loading, setLoading] = useState(true)
  useEffect(() => {
    setTimeout(() => setLoading(false), 500)
}, [])
  const content = (
    <>
      <div className="main-panel">
        <nav className="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
          <div className="container-fluid">
            <div className="navbar-wrapper">
              <div className="navbar-toggle">
                {/* <Button type="button" className="navbar-toggler" onClick={() => setExpandSidebar(true)}>
                      <span className="navbar-toggler-bar bar1"></span>
                      <span className="navbar-toggler-bar bar2"></span>
                      <span className="navbar-toggler-bar bar3"></span>
                    </Button> */}
              </div>
              <a className="navbar-brand" href="javascript:;">
                THỐNG KÊ VÀNG
              </a>
            </div>
            <button
              className="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navigation"
              aria-controls="navigation-index"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-bar navbar-kebab"></span>
              <span className="navbar-toggler-bar navbar-kebab"></span>
              <span className="navbar-toggler-bar navbar-kebab"></span>
            </button>
            <div
              className="collapse navbar-collapse justify-content-end"
              id="navigation"
            >
              <div className="navbar-nav">
                <div className="nav-item" >
                  {/* <Button onClick={() => setIsToday(true)}>{" "} Xem hôm nay</Button>
    
                      <Button onClick={() => setIsPrint(true)}><i className="fa fa-print" aria-hidden="true"></i>{" "} In</Button>
                      <Button onClick={() => {
                        handleExportExcel()
                      }}>Xuất thống kê</Button>
                      <ConfigProvider locale={viVN}>
                        <DatePicker picker="month" onChange={handleChangeMonth} format={"MM/YYYY"} locale={locale} />
                      </ConfigProvider> */}
                </div>

              </div>

            </div>
          </div>
        </nav>
        {loading ? (
          <div className="loading">
            <Lottie
              loop
              path="/img/AnimationLoading.json"
              play
              style={{ width: 300, height: 300 }}
            />
          </div>
        ) : (
          <div className="content">

            <div className="row">
              <div className="col-lg-4 col-md-6 col-sm-6">
                <div className="card card-stats">
                  <div className="card-body ">
                    <div className="row">
                      <div className="col-5 col-md-4">
                        <div className="icon-big text-center icon-warning">
                          <i className="nc-icon nc-globe text-warning"></i>
                        </div>
                      </div>
                      <div className="col-7 col-md-8">
                        <div className="numbers">
                          <p className="card-category">Thu</p>
                          <p className="card-title">
                            {/* <FormatCurrency amount={overView.Collect} /> */}
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card-footer ">
                    <hr />
                    <div className="stats">
                      <i className="fa fa-calendar-check-o"></i>
                      {/* Tháng {currentMonth + "/" + currentYear} */}
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg-4 col-md-6 col-sm-6">
                <div className="card card-stats">
                  <div className="card-body ">
                    <div className="row">
                      <div className="col-5 col-md-4">
                        <div className="icon-big text-center icon-warning">
                          <i className="nc-icon nc-money-coins text-success"></i>
                        </div>
                      </div>
                      <div className="col-7 col-md-8">
                        <div className="numbers">
                          <p className="card-category">Chi</p>
                          <p className="card-title">
                            {/* <FormatCurrency amount={overView.Spend} /> */}
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card-footer ">
                    <hr />
                    <div className="stats">
                      <i className="fa fa-calendar-check-o"></i>
                      {/* Tháng {currentMonth + "/" + currentYear} */}
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg-4 col-md-6 col-sm-6">
                <div className="card card-stats">
                  <div className="card-body ">
                    <div className="row">
                      <div className="col-5 col-md-4">
                        <div className="icon-big text-center icon-warning">
                          <i className="nc-icon nc-vector text-danger"></i>
                        </div>
                      </div>
                      <div className="col-7 col-md-8">
                        <div className="numbers">
                          <p className="card-category">Lợi nhuận</p>
                          <p className="card-title">
                            {/* <FormatCurrency amount={overView.Revenue} /> */}
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card-footer ">
                    <hr />
                    <div className="stats">
                      <i className="fa fa-calendar-check-o"></i>
                      {/* Tháng {currentMonth + "/" + currentYear} */}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-12">
                <div className="card card-chart">
                  <div className="card-header ">
                    <h5 className="card-title">LỢI NHUẬN</h5>
                    <p className="card-category">Trong tháng </p>
                  </div>
                  <div className="card-body ">
                    {/* <RevenueTable month={currentMonth} year={currentYear} isOrder={0} /> */}
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-6">
                <div className="card card-chart">
                  <div className="card-header ">
                    <h5 className="card-title">CHI TIẾT KHOẢN THU</h5>
                    <p className="card-category">
                      {/* {isTable == 0
                          ? "Thông tin đóng lãi"
                          : "Thông tin thanh toán"}{" "}
                        trong tháng {currentMonth} */}
                    </p>
                  </div>
                  <div className="card-body ">
                    {/* <CollectTable
                        month={currentMonth}
                        year={currentYear}
                        isTable={isTable}
                        setIsTable={setIsTable}
                      /> */}
                  </div>
                  <div className="card-footer ">
                    <hr />
                    <div className="footer-overview">
                      {/* <Button
                          className="btn-payment"
                          onClick={() => setIsTable(0)}
                        >
                          Thông tin đóng lãi
                        </Button>
                        <Button
                          className="btn-redeem"
                          onClick={() => setIsTable(1)}
                        >
                          Thông tin thanh toán
                        </Button> */}
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-6">
                <div className="card card-chart">
                  <div className="card-header">
                    <h5 className="card-title">CHI TIẾT KHOẢN CHI</h5>
                    <p className="card-category">Trong tháng</p>
                  </div>
                  <div className="card-body">
                    {/* <SpendTableGold month={currentMonth} year={currentYear} isOrder={0} /> */}
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}

      </div>
    </>
  );
  return (<TheContent content={content} active={1} />)
}