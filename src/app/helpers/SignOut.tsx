"use client"
const clearToken = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('name');
};

const SignOut = () => {
    clearToken();
    window.location.href = '/';
};

export default SignOut
