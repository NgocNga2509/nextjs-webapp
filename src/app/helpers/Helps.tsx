import dayjs from "dayjs";

export const FormatCurrency = ({ amount }: any) => {
  const formatter = new Intl.NumberFormat("vi-VN", {
    style: "currency",
    currency: "VND",
    minimumFractionDigits: 0,
  });
  return formatter.format(amount);
};
export const FormatAmountIntToFloat = (value: any) => {
  if (value != "") {
    // Xóa các dấu phân tách hiện có trong giá trị
    const cleanedValue = value.replace(/,/g, "");

    // Chuyển đổi thành số nguyên
    const intValue = parseInt(cleanedValue);

    // Kiểm tra nếu không phải số hợp lệ
    if (isNaN(intValue)) {
      return "";
    }

    const floatValue = intValue.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

    return floatValue;

  }
};
export const FormatAmountFloatToInt = (value: any) => {
  if (value != "") {
    // Xóa các dấu phân tách
    const cleanedValue = value.replace(/,/g, "");

    // Chuyển đổi thành số nguyên
    const intValue = parseInt(cleanedValue);

    // Kiểm tra nếu không phải số hợp lệ
    if (isNaN(intValue)) {
      return "";
    }
    return parseInt(intValue.toString());
  }
};

export const ConvertNextMonth = (day: any) => {
  const date = dayjs(day);
  // Lấy ngày sau 1 tháng
  const nextMonth = date.add(1, "month");

  // Lấy thông tin ngày, tháng, năm từ ngày sau 1 tháng
  const nextMonthDay = nextMonth.date();
  const nextMonthMonth = nextMonth.month() + 1; // Tháng trong Day.js bắt đầu từ 0, nên cần cộng thêm 1
  const nextMonthYear = nextMonth.year();
  // Định dạng lại thành dd/mm/yyyy
  const nextMonthFormatted = `${nextMonthDay < 10 ? "0" + nextMonthDay : nextMonthDay
    }/${nextMonthMonth < 10 ? "0" + nextMonthMonth : nextMonthMonth
    }/${nextMonthYear}`;

  console.log(nextMonthFormatted);
  const dateS = dayjs(nextMonthFormatted, "DD/MM/YYYY");

  const isoString = dateS.toISOString();

  return isoString;
};
export const FormatDate = ({ date }: any) => {
  const dateObject = new Date(date);

  const day = dateObject.getDate();
  const month = dateObject.getMonth() + 1;
  const year = dateObject.getFullYear();

  return `${day < 10 ? "0" + day : day}/${month < 10 ? "0" + month : month
    }/${year}`;
};
