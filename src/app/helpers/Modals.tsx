"use client"
import { Button, Modal } from "antd";
import Link from "next/link";

export default function ModalSuccess({ isOpen, setIsOpen }: any) {
    const handleCancel = () => {
        setIsOpen(false)
    }

    return (
        <Modal
            open={isOpen}
            onCancel={handleCancel}
            zIndex={1030}
            footer={
                <>
                    <Button className="" onClick={handleCancel}>Hủy</Button>
                    <Link href="/order/list">
                        <Button className="bg-green-500">OK</Button>
                    </Link>
                </>
            }
        >
            Bạn có muốn chuyển qua trang Quản lý đơn?
        </Modal>
    )
}
export const showSuccessCreate = (isCreatePawn:boolean) => {
    return (
        Modal.success({
            title: 'Bạn có muốn chuyển qua trang Quản lý?',
            footer: () => (
                <>
                    <div className="d-flex justify-content-right">
                        <Link href={isCreatePawn ? "/order/create" : "/customer/create"}>
                            <Button>Hủy</Button>
                        </Link>
                        <div className="btn-success-create">
                            <Link href={isCreatePawn ? "/order/list" : "/customer/list"}>
                                <Button className="btn-success-create-label">OK</Button>
                            </Link>
                        </div>
                    </div>
                </>
            ),
        })
    )
};
export const showDeleteConfirm = (handleDelete: any) => {
    Modal.confirm({
        title: 'Bạn có muốn xóa Đơn cầm này?',
        onOk: () => {
            handleDelete();
        },
    });
};