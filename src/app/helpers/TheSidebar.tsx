"use client"
import React from "react";
import Link from 'next/link';
import Image from 'next/image';
import SignOut from "./SignOut";

const TheSideBar = ({ active }: any) => {
  return (
    <>
      <div className="sidebar" data-color="white" data-active-color="danger">
        <div className="logo">
          <Image
            src="/KH.png" // Đường dẫn hình ảnh trong thư mục `public`
            alt="Mô tả hình ảnh"
            width={250}
            height={250}
          />
        </div>
        <div className="sidebar-wrapper">
          <ul className="nav">
            <li className={active == 1 ? "active" : ""}>
              <Link href="/dashboard">
                <i className="fa fa-university" aria-hidden="true"></i>
                <p>Thống kê</p>
              </Link>
            </li>

            <li className={active == 2 ? "active" : ""}>
              <Link href="/order/create">
                <i className="fa fa-diamond" aria-hidden="true"></i>
                <p>Thêm đơn vàng</p>
              </Link>
            </li>

            <li className={active == 3 ? "active" : ""}>
              <Link href="/order/list">
                <i className="fa fa-th-list" aria-hidden="true"></i>
                <p>Quản lý đơn</p>
              </Link>
            </li>
            <li className={active == 4 ? "active" : ""}>
              <Link href="/customer/create">
                <i className="fa fa-user-plus" aria-hidden="true"></i>
                <p>Thêm khách hàng</p>
              </Link>
            </li>
            <li className={active == 5 ? "active" : ""}>
              <Link href="/customer/list">
                <i className="fa fa-users" aria-hidden="true"></i>
                <p>Quản lý khách hàng</p>
              </Link>
            </li>
            <li className={active == 6 ? "active" : ""}>
              <Link href="/category">
                <i className="fa fa-list-alt" aria-hidden="true"></i>
                <p>Danh mục</p>
              </Link>
            </li>
          </ul>

          <footer className="footer">
            <div className="container-fluid">
              <div className="row">
                <nav className="footer-nav">
                  <ul>
                    <li>
                      <a>
                        Đổi mật khẩu
                      </a>
                    </li>
                    <li onClick={SignOut}>
                      <a>
                        Đăng xuất
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          </footer>
        </div>

      </div>
    </>
  );
};
export default TheSideBar;
