"use client"
import { useState } from "react";
import TheSideBar from "./TheSidebar";


const TheContent = ({ content, active}:any) => {
//   const [active, setActive] = useState(1);
  const [numberNotification, setNumberNotification]=useState(0)
  const [numberNotificationVehicle, setNumberNotificationVehicle]=useState(0)

  return (
      <div className="wrapper">
        <TheSideBar active={active}/>
        <div>{content}</div>
      </div>
  );
};
export default TheContent;
