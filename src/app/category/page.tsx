"use client"
import { useEffect, useState } from "react";
import { Button } from "antd";
import { Spin } from "antd";
import CategoryProduct from "./component/CategoryProduct";
import CategoryGold from "./component/CategoryGold";
import CategoryPercent from "./component/CategoryPercent";
import TheContent from "../helpers/TheContent";
import BaseURL from "../api/BaseURL";
import Lottie from "react-lottie-player";

interface Category {
  category_id: "",
  category_name: ""
}
interface CategoryPercent {
  percent_id: "",
  percent: ""
}
const Category = () => {
  const [loading, setLoading] = useState(true);
  const [subMenu, setSubMenu] = useState(false);
  const [active, setActive] = useState(9);
  const [data, setData] = useState<Category[]>([]);
  const [dataGoal, setDataGoal] = useState<Category[]>([]);
  const [dataPercent, setDataPercent] = useState<CategoryPercent[]>([]);
  const [expandSidebar, setExpandSidebar] = useState(false)

  const [resetFlag, setResetFlag] = useState(1);
  const [categoryName, setCategoryName] = useState("");

  const dataSource = data.map((item, index) => ({
    key: index,
    CategoryID: item.category_id,
    CategoryName: item.category_name,
  }));

  const dataSourceGoal = dataGoal.map((item, index) => ({
    key: index,
    CategoryID: item.category_id,
    CategoryName: item.category_name,
  }));

  const dataSourcePercent = dataPercent.map((item, index) => ({
    key: index,
    PercentID: item.percent_id,
    Percent: item.percent,
  }));

  const handleSubmit = () => {
    const param = {
      category_name: categoryName,
    };
    BaseURL
      .post("create-category", param)
      .then((response) => {
        if (response.status === 200) {
          setResetFlag(resetFlag + 1);
        }
      });
  };

  const handleSubmitGoal = () => {
    const param = {
      category_name: categoryName,
    };
    BaseURL
      .post("create-category-gold", param)
      .then((response) => {
        if (response.status === 200) {
          setResetFlag(resetFlag + 1);
        }
      });
  };

  useEffect(() => {
    //setLoading(true);
    BaseURL
      .get("get-list-category")
      .then((response) => {
        if (response.status === 200) {
          setData(response.data);
          setLoading(false)
        }
      })
      .catch((error) => {
        if (error.response) {
          console.log("Server responded with a non-2xx status");
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          console.log("No response received from the server");
          console.log(error.request);
        } else {
          console.log("Error setting up the request");
          console.log(error.message);
        }
        console.log(error.config);
      });
    BaseURL
      .get("get-list-category-gold")
      .then((response) => {
        if (response.status === 200) {
          setDataGoal(response.data);
          setLoading(false)
        }
      })
      .catch((error) => {
        if (error.response) {
          console.log("Server responded with a non-2xx status");
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          console.log("No response received from the server");
          console.log(error.request);
        } else {
          console.log("Error setting up the request");
          console.log(error.message);
        }
        console.log(error.config);
      });
    BaseURL
      .get("get-list-category-percent")
      .then((response) => {
        if (response.status === 200) {
          setDataPercent(response.data);
          setLoading(false)
        }
      })
      .catch((error) => {
        if (error.response) {
          console.log("Server responded with a non-2xx status");
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          console.log("No response received from the server");
          console.log(error.request);
        } else {
          console.log("Error setting up the request");
          console.log(error.message);
        }
        console.log(error.config);
      });
  }, [resetFlag]);

  const content = (
    <>
      <div className="main-panel">
        <nav className="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
          <div className="container-fluid">
            <div className="navbar-wrapper">
              <div className="navbar-toggle">
                <Button className="navbar-toggler" onClick={() => setExpandSidebar(true)}>
                  <span className="navbar-toggler-bar bar1"></span>
                  <span className="navbar-toggler-bar bar2"></span>
                  <span className="navbar-toggler-bar bar3"></span>
                </Button>
              </div>
              <a className="navbar-brand" href="javascript:;">
                DANH MỤC
              </a>
            </div>
          </div>
        </nav>
        <div className="content">
          {loading ? (
            <div className="loading">
              <Lottie
                loop
                path="/img/AnimationLoading.json"
                play
                style={{ width: 300, height: 300 }}
              />
            </div>
          ) : (
            <div className="row">
              <div className="col-md-12">
                <div className="card">
                  <div className="card-header">
                    {/* <h4 className="card-title">DANH MỤC HÀNG</h4> */}
                  </div>
                  <div className="card-body">
                    <div className="category">

                      <CategoryProduct
                        setCategoryName={setCategoryName}
                        handleSubmit={handleSubmit}
                        dataSource={dataSource}
                        resetFlag={resetFlag}
                        setResetFlag={setResetFlag}
                      />
                      <CategoryGold
                        setCategoryName={setCategoryName}
                        handleSubmitGoal={handleSubmitGoal}
                        dataSourceGoal={dataSourceGoal}
                        resetFlag={resetFlag}
                        setResetFlag={setResetFlag}
                      />
                      <CategoryPercent
                        dataSource={dataSourcePercent}
                        resetFlag={resetFlag}
                        setResetFlag={setResetFlag}
                      />
                    </div>
                    {/* } */}
                  </div>
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    </>
  );
  return (
    <TheContent
      content={content}
      active={6}
    />
  );
};
export default Category;
