"use client"
import { Form, Input, Button, Checkbox } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { useRouter } from 'next/navigation';
import Image from 'next/image';
import { useState } from 'react';
import { toast, ToastContainer } from 'react-toastify'
import "react-toastify/dist/ReactToastify.css";
import axios from 'axios';

export default function SignIn() {
    const [form] = Form.useForm();

    const [formData, setFormData] = useState({
        username: "",
        password: ""
    })
    console.log(formData);

    const router = useRouter();

    const handleChange = (e: any) => {
        setFormData({ ...formData, [e.target.name]: e.target.value })
    }
    const handleSubmit = () => {
        const params = {
            username: formData.username,
            password: formData.password
        }
        axios.post("http://localhost:5000/api/sign-in", params).then((response) => {
            if (response.status === 200) {
                console.log(response);
                localStorage.setItem("token", response.data.token);
                localStorage.setItem("name", response.data.username);
                router.push('/dashboard')
            }
        }).catch((err) => toast("Đăng nhập thất bại!"));
    }
    return (
        <>
            <div className="login-container">
                <div className='login-form'>
                    <Form
                        name="normal_login"
                        className="login-form-cus"
                        initialValues={{
                            remember: true,
                        }}
                        autoComplete='off'
                        form={form}
                    // onFinish={onFinish}
                    >
                        <div className="logo">
                            <Image
                                src="/KH.png" // Đường dẫn hình ảnh trong thư mục `public`
                                alt="Mô tả hình ảnh"
                                width={250}
                                height={250}
                            />
                        </div>
                        <Form.Item
                            name="username"
                            rules={[
                                {
                                  required: true,
                                  message: 'Vui lòng nhập tên đăng nhập!',
                                },
                              ]}
                        >
                            <Input
                                className="login-input"
                                name="username"
                                prefix={<UserOutlined className="site-form-item-icon" />}
                                placeholder="Tên đăng nhập" onChange={handleChange} />
                        </Form.Item>
                        <Form.Item
                            name="password"
                            rules={[
                                {
                                  required: true,
                                  message: 'Vui lòng nhập mật khẩu!',
                                },
                              ]}
                        >
                            <Input.Password
                                name="password"
                                className="login-input"
                                onChange={handleChange}
                                prefix={<LockOutlined className="site-form-item-icon" />}
                                type="password"
                                placeholder="Mật khẩu"
                            />
                        </Form.Item>
                        <Form.Item>
                            <a className="login-form-forgot" href="">
                                Quên mật khẩu?
                            </a>
                        </Form.Item>

                        <Form.Item>
                            <Button
                                type="primary"
                                //onClick={handleSubmit}
                                onClick={() => {
                                    form
                                        .validateFields()
                                        .then(() => {
                                            handleSubmit();
                                        })
                                        .catch((errorInfo) => {
                                            console.log('Validation failed:', errorInfo);
                                        });
                                }}
                                htmlType="submit"
                                className="login-form-button"
                            >
                                Đăng nhập
                            </Button>
                        </Form.Item>
                    </Form>
                </div>
                <ToastContainer autoClose={1000} theme="dark" position="top-center" />
            </div>
        </>
    );
}
