"use client"
import {
    Button,
    ConfigProvider,
    DatePicker,
    Form,
    Input,
    Select,
    Table,
} from "antd";
import TextArea from "antd/es/input/TextArea";
import { useEffect, useState } from "react";
import { ToastContainer, toast } from "react-toastify";
import locale from 'antd/es/date-picker/locale/vi_VN';
import viVN from "antd/lib/locale/vi_VN";
import moment from "moment";
import 'dayjs/locale/vi';
import TheContent from "@/app/helpers/TheContent";
import BaseURL from "@/app/api/BaseURL";
import { showSuccessCreate } from "@/app/helpers/Modals";
import Lottie from "react-lottie-player";
const NewCustomer = () => {
    const [loading, setLoading]=useState(true)
    const [active, setActive] = useState(8);
    const [subMenu, setSubMenu] = useState(true);
    const [expandSidebar, setExpandSidebar] = useState(false)
    const [form] = Form.useForm();
    useEffect(() => {
        setTimeout(() => setLoading(false), 500)
    }, [])
    const [formData, setFormData] = useState({
        customer_name: "",
        cccd: "",
        gender: "",
        address: "",
        birth_date: null as string | null,
        phone: "",
        day_cccd: null as string | null,
        address_cccd: "CT CỤC CSHCQTTHXH",
    });
    const handleChange = (e: any) => {
        setFormData({ ...formData, [e.target.name]: e.target.value });
    };

    const handleChangeDate = (date: any, dateString: any) => {
        const momentObj = moment(dateString, "YYYY-MM-DD");

        // Create the target string with the expected format
        const targetString = momentObj.format("YYYY-MM-DDTHH:mm:ss[Z]");
        setFormData({ ...formData, birth_date: targetString });
    };
    const handleChangeDateCCCD = (date: any, dateString: any) => {
        // Parse the original string as a moment object
        const momentObj = moment(dateString, "YYYY-MM-DD");

        // Create the target string with the expected format
        const targetString = momentObj.format("YYYY-MM-DDTHH:mm:ss[Z]");
        setFormData({ ...formData, day_cccd: targetString });
    };

    const onSelectGender = (e: any) => {
        setFormData({ ...formData, gender: e });
    };

    const handleSubmit = () => {
        const params = {
            customer_name: formData.customer_name,
            cccd: formData.cccd,
            gender: formData.gender,
            address: formData.address,
            birth_date: formData.birth_date,
            phone: formData.phone,
            day_cccd: formData.day_cccd,
            address_cccd: formData.address_cccd,
        };
        BaseURL.post("create-customer", params)
            .then((response: any) => {
                console.log(response);
                if (response.status === 200) {
                    showSuccessCreate(false)
                }
            })
            .catch((err: any) => toast("Thêm khách hàng thất bại!"));
    };
    const content = (
        <div className="main-panel">
            <nav className="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
                <div className="container-fluid">
                    <div className="navbar-wrapper">
                        <div className="navbar-toggle">
                            <Button
                                // type="button"
                                className="navbar-toggler" onClick={() => setExpandSidebar(true)}>
                                <span className="navbar-toggler-bar bar1"></span>
                                <span className="navbar-toggler-bar bar2"></span>
                                <span className="navbar-toggler-bar bar3"></span>
                            </Button>
                        </div>
                        <a className="navbar-brand" href="javascript:;">
                            KHÁCH HÀNG
                        </a>
                    </div>
                </div>
            </nav>
            <div className="content">
                {loading ? (
                    <div className="loading">
                        <Lottie
                            loop
                            path="/img/AnimationLoading.json"
                            play
                            style={{ width: 300, height: 300 }}
                        />
                    </div>
                ) : (
                    <div className="row">
                        <div className="col-md-8">
                            <div className="card ">
                                <div className="card-header ">
                                    <Form className="form-add-pawn" autoComplete="off" autoSave="off" form={form}>
                                        <Form.Item
                                            className="form-add-pawn-item"
                                            label="Tên khách hàng"
                                            name="customer_name"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập tên Khách hàng',
                                                },
                                            ]}
                                        >
                                            <Input
                                                className="form-input-pawn"
                                                onChange={handleChange}
                                                name="customer_name"
                                            />
                                        </Form.Item>
                                        <Form.Item className="form-add-pawn-item" label="Giới tính">
                                            <Select
                                                placeholder="Chọn giới tính"
                                                className="form-input-select"
                                                onSelect={onSelectGender}
                                            >
                                                <Select.Option value="0">Nữ</Select.Option>
                                                <Select.Option value="1">Nam</Select.Option>
                                            </Select>
                                        </Form.Item>
                                        <Form.Item
                                            className="form-add-pawn-item"
                                            label="Ngày sinh"
                                        >
                                            <ConfigProvider locale={viVN}>
                                                <DatePicker
                                                    className="form-input-pawn"
                                                    onChange={handleChangeDate}
                                                    name="birth_date"
                                                    locale={locale}
                                                />
                                            </ConfigProvider>
                                        </Form.Item>
                                        <Form.Item
                                            className="form-add-pawn-item"
                                            label="CMND/CCCD"
                                            name="cccd"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập tên CMND/CCCD',
                                                },
                                            ]}>
                                            <Input
                                                className="form-input-pawn"
                                                onChange={handleChange}
                                                name="cccd"
                                            />
                                        </Form.Item>
                                        <Form.Item className="form-add-pawn-item" label="Ngày cấp">
                                            <ConfigProvider locale={viVN}>
                                                <DatePicker className="form-input-pawn"
                                                    onChange={handleChangeDateCCCD}
                                                    name="day_cccd" />
                                            </ConfigProvider>
                                        </Form.Item>
                                        <Form.Item className="form-add-pawn-item" label="Nơi cấp" >
                                            <Input
                                                defaultValue={formData.address_cccd}
                                                className="form-input-pawn"
                                                onChange={handleChange}
                                                name="address_cccd"
                                            />
                                        </Form.Item>
                                        <Form.Item
                                            className="form-add-pawn-item"
                                            label="Số điện thoại"
                                            name="phone"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập Số điện thoại',
                                                },
                                            ]}
                                        >
                                            <Input
                                                className="form-input-pawn"
                                                onChange={handleChange}
                                                name="phone"
                                            />
                                        </Form.Item>
                                        <Form.Item className="form-add-pawn-item" label="Hộ khẩu thường trú">
                                            <TextArea
                                                className="form-input-textarea"
                                                onChange={handleChange}
                                                name="address"
                                            />
                                        </Form.Item>

                                        <div className="save-form-pawn">
                                            <div className="update ml-auto mr-auto">
                                                <button
                                                    onClick={() => {
                                                        form
                                                            .validateFields()
                                                            .then(() => {
                                                                handleSubmit();
                                                            })
                                                            .catch((errorInfo) => {
                                                                console.log('Validation failed:', errorInfo);
                                                            });
                                                    }}
                                                    className="btn btn-primary btn-round"

                                                >
                                                    Lưu
                                                </button>
                                            </div>
                                        </div>
                                    </Form>
                                </div>
                                <div className="card-body ">
                                    <canvas id="chartHours" width="400" height="100"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                )}
            </div>

        </div>
    );
    return (
        <>
            <TheContent
                content={content}
                active={4}
            />

            <ToastContainer autoClose={1000} theme="dark" position="top-center" />
        </>
    );
}
export default NewCustomer