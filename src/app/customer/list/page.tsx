"use client"
import { useEffect, useState } from "react";
import TheContent from "../../helpers/TheContent";
import { Button, Input, Pagination, Select, Table } from "antd";
import { ToastContainer, toast } from "react-toastify";
import BaseURL from "@/app/api/BaseURL";
import { FormatDate } from "@/app/helpers/Helps";
import Lottie from "react-lottie-player";
interface CustomerList {
  customer_name: "",
  customer_id: "",
  cccd: "",
  gender: "",
  address: "",
  birth_date: null,
  phone: "",
  day_cccd: null,
  address_cccd: "CT CỤC CSHCQTTHXH",
}
export default function ListCustomer() {
  const [loading, setLoading] = useState(true)
  const [active, setActive] = useState(7);
  const [subMenu, setSubMenu] = useState(true);
  const [data, setData] = useState<CustomerList[]>([]);
  const [currentPage, setCurrentPage] = useState(1); // Trang hiện tại
  const pageSize = 10; // Kích thước trang
  const [selectSearch, setSelectSearch] = useState(0);
  const [isModalOrder, setIsModalOrder] = useState(false);
  const [isDrawerDetail, setIsDrawerDetail] = useState(false);
  const [customerId, setCustomerId] = useState("");
  const [resetFlag, setResetFlag] = useState(1);
  const [modalDelete, setModalDelete] = useState(false);
  const [expandSidebar, setExpandSidebar] = useState(false)

  useEffect(() => {
    setLoading(true)
    BaseURL.get("list-customer").
      then((response) => {
        console.log(response);
        if (response.status == 200) {
          setData(response.data)
          setLoading(false)
        }
      }).catch((err) => console.log(err));
  }, [])
  const columns = [
    {
      title: "Mã khách hàng",
      dataIndex: "CustomerID",
      key: "CustomerID",
    },
    {
      title: "Họ và tên",
      dataIndex: "CustomerName",
      key: "CustomerName",
    },
    {
      title: "Ngày tháng năm sinh",
      dataIndex: "BirthDate",
      key: "BirthDate",
    },
    {
      title: "Giới tính",
      dataIndex: "Gender",
      key: "Gender",
      render: (gender: any) => convertGender(gender),
    },
    {
      title: "CMND/CCCD",
      dataIndex: "CCCD",
      key: "CCCD",
    },
    {
      title: "Ngày cấp:",
      dataIndex: "DayCCCD",
      key: "DayCCCD",
    },
    {
      title: "Nơi cấp:",
      dataIndex: "AddressCCCD",
      key: "AddressCCCD",
    },
    {
      title: "SĐT",
      dataIndex: "Phone",
      key: "Phone",
    },
    {
      title: "Địa chỉ",
      dataIndex: "Address",
      key: "Address",
    },
    {
      title: "",
      dataIndex: "CustomerID",
      key: "CustomerID",
      render: (id: any) => (
        <div className="button-action">
          <Button
            className="button-action-edit"
            onClick={() => {
              setIsDrawerDetail(true);
              setCustomerId(id);
            }}
          >
            Sửa
          </Button>
          <Button
            className="button-action-delete"
            onClick={() => {
              setModalDelete(true);
              setCustomerId(id);
            }}
          >
            Xóa
          </Button>
          <Button
            className="button-action-detail"
            onClick={() => {
              setIsModalOrder(true);
              setCustomerId(id);
            }}
          >
            Thông tin giao dịch
          </Button>
        </div>
      ),
    },
  ];

  const dataSource = data.map((item, index) => ({
    key: index,
    CustomerName: item.customer_name,
    CustomerID: item.customer_id,
    BirthDate: item.birth_date && <FormatDate date={item.birth_date} />,
    Gender: item.gender,
    Phone: item.phone,
    CCCD: item.cccd,
    Address: item.address,
    DayCCCD: item.day_cccd,
    AddressCCCD: item.address_cccd,
  }));
  // Số lượng tổng cộng các mục dữ liệu
  const totalItems = dataSource.length;
  const handlePageChange = (page: any) => {
    setCurrentPage(page);
  };
  // Lấy dữ liệu trang hiện tại
  const currentPageData = dataSource.slice(
    (currentPage - 1) * pageSize,
    currentPage * pageSize
  );


  const convertGender = (gender: any) => {
    if (gender == "") {
      return ""
    } else if (gender === "0") {
      return "Nữ";
    } else {
      return "Nam";
    }
  };
  const handleChangeSelectSearch = (e: any) => {
    setSelectSearch(e);
  };

  const [searchContent, setSearchContent] = useState("");

  // const handleDelete = (id) => {
  //   BaseURL
  //     .delete(`delete-customer/${id}`)
  //     .then((response) => {
  //       if (response.status === 200) {
  //         setResetFlag(resetFlag + 1);
  //         setModalDelete(false)
  //       }
  //     })
  //     .catch((error) =>toast("Khách hàng này có đơn hàng. Bạn không thể xóa khách hàng này!"));
  // };
  const content = (
    <>
      <div className="main-panel">
        <nav className="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
          <div className="container-fluid">
            <div className="navbar-wrapper">
              <div className="navbar-toggle">
                <Button className="navbar-toggler" onClick={() => setExpandSidebar(true)}>
                  <span className="navbar-toggler-bar bar1"></span>
                  <span className="navbar-toggler-bar bar2"></span>
                  <span className="navbar-toggler-bar bar3"></span>
                </Button>
              </div>
              <a className="navbar-brand" href="javascript:;">
                KHÁCH HÀNG
              </a>
            </div>
          </div>
        </nav>
        <div className="content">
          {loading ? (
            <div className="loading">
              <Lottie
                loop
                path="/img/AnimationLoading.json"
                play
                style={{ width: 300, height: 300 }}
              />
            </div>
          ) : (
            <div className="row">
              <div className="col-md-12">
                <div className="card">
                  <div className="card-header">
                    <form>
                      <div className="search-customer">
                        <Input
                          type="text"
                          className="form-control"
                          placeholder="Tìm kiếm..."
                          onChange={(e) => setSearchContent(e.target.value)}
                        />
                        <div className="input-group-append">
                          <div className="input-group-text">
                            <i className="nc-icon nc-zoom-split"></i>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div className="card-body">
                    <div className="table-responsive">
                      <Table
                        columns={columns}
                        dataSource={currentPageData}
                        pagination={false}
                      />
                      <Pagination showSizeChanger={false}
                        className="pagination-category"
                        current={currentPage}
                        pageSize={pageSize}
                        total={totalItems}
                        onChange={handlePageChange}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    </>
  );
  return (
    <>
      <TheContent
        content={content}
        active={5}
      />
      {/* {isModalOrder && (
          <ModalOrder
            customerID={customerId}
            isOpen={isModalOrder}
            setIsOpen={setIsModalOrder}
          />
        )}
        {isDrawerDetail && (
          <DrawerEditCustomer
            isOpen={isDrawerDetail}
            setIsOpen={setIsDrawerDetail}
            customerID={customerId}
            resetFlag={resetFlag}
            setResetFlag={setResetFlag}
          />
        )}
        {modalDelete && (
          <ModalDeleteCustomer
            isOpen={modalDelete}
            setIsOpen={setModalDelete}
            handleDelete={handleDelete}
            id={customerId}
          />
        )} */}
      <ToastContainer autoClose={1000} theme="dark" position="top-center" />
    </>
  );
}