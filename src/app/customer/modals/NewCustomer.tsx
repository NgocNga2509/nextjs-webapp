"use client"

import { ConfigProvider, DatePicker, Form, Input, Modal, Select } from "antd"
import TextArea from "antd/es/input/TextArea";
import { useEffect, useState } from "react";
import locale from 'antd/es/date-picker/locale/vi_VN';
import viVN from "antd/lib/locale/vi_VN";
import 'dayjs/locale/vi';
import moment from "moment";
import { ToastContainer, toast } from "react-toastify";
import BaseURL from "@/app/api/BaseURL";
export default function NewCustomerModal({
    isOpen,
    setIsOpen,
    isSelect,
    setIsSelect,
    resetNewCustomer,
    setResetNewCustomer,
}: any) {
    const [form] = Form.useForm();
    const [formData, setFormData] = useState({
        customer_name: "",
        cccd: "",
        gender: "",
        address: "",
        birth_date: null as string | null,
        phone: "",
        day_cccd: null as string | null,
        address_cccd: "CT CỤC CSHCQTTHXH"
    });
    const handleCancel = () => {
        setIsOpen(false)
    }
    const handleOk = () => {
        const params = {
            customer_name: formData.customer_name,
            cccd: formData.cccd,
            gender: formData.gender,
            address: formData.address,
            birth_date: formData.birth_date,
            phone: formData.phone,
            day_cccd: formData.day_cccd,
            address_cccd: formData.address_cccd,
        };
        if (params.customer_name == "") toast("Vui lòng nhập tên khách hàng")
        else {
            BaseURL.post("create-customer", params)
                .then((response: any) => {
                    if (response.status === 200) {
                        setIsSelect(true);
                        setIsOpen(false);
                        setResetNewCustomer(resetNewCustomer + 1)
                    }
                })
                .catch((err: any) => toast("Thêm khách hàng thất bại!"));
        }
    };

    const handleChange = (e: any) => {
        setFormData({ ...formData, [e.target.name]: e.target.value });
    };

    const handleChangeDate = (date: any, dateString: any) => {
        const momentObj = moment(dateString, "YYYY-MM-DD");

        // Create the target string with the expected format
        const targetString = momentObj.format("YYYY-MM-DDTHH:mm:ss[Z]");
        setFormData({ ...formData, birth_date: targetString });
    };

    const onSelectGender = (e: any) => {
        setFormData({ ...formData, gender: e });
    };

    const handleChangeDateCCCD = (date: any, dateString: any) => {
        // Parse the original string as a moment object
        const momentObj = moment(dateString, "YYYY-MM-DD");

        // Create the target string with the expected format
        const targetString = momentObj.format("YYYY-MM-DDTHH:mm:ss[Z]");
        setFormData({ ...formData, day_cccd: targetString });
    };

    return (<>
        <Modal
            className="modal-list-customer"
            title="Thêm khách hàng"
            open={isOpen}
            onCancel={handleCancel}
            //onOk={handleOk}
            onOk={() => {
                form
                    .validateFields()
                    .then(() => {
                        handleOk();
                    })
                    .catch((errorInfo) => {
                        console.log('Validation failed:', errorInfo);
                    });
            }}
            zIndex={1030}
            width={1000}
            bodyStyle={{ maxHeight: '500px', overflow: 'auto' }}
        >
            <>
                <Form className="form-add-pawn" autoComplete="off" form={form}>
                    <Form.Item
                        className="form-add-pawn-item"
                        label="Tên khách hàng"
                        name="customer_name"
                        rules={[
                            {
                                required: true,
                                message: 'Vui lòng nhập tên Khách hàng',
                            },
                        ]}>
                        <Input
                            className="form-input-pawn"
                            onChange={handleChange}
                            name="customer_name"
                        />
                    </Form.Item>
                    <Form.Item className="form-add-pawn-item" label="Giới tính">
                        <Select
                            placeholder="Chọn giới tính"
                            className="form-input-select"
                            onSelect={onSelectGender}
                        >
                            <Select.Option value="0">Nữ</Select.Option>
                            <Select.Option value="1">Nam</Select.Option>
                        </Select>
                    </Form.Item>
                    <Form.Item
                        className="form-add-pawn-item"
                        label="Ngày sinh"
                    >
                        <ConfigProvider locale={viVN}>
                            <DatePicker
                                className="form-input-pawn"
                                onChange={handleChangeDate}
                                name="birth_date"
                                locale={locale}
                            />
                        </ConfigProvider>
                    </Form.Item>
                    <Form.Item
                        className="form-add-pawn-item"
                        label="CMND/CCCD"
                        name="cccd"
                        rules={[
                            {
                                required: true,
                                message: 'Vui lòng nhập tên CMND/CCCD',
                            },
                        ]}>
                        <Input
                            className="form-input-pawn"
                            onChange={handleChange}
                            name="cccd"
                        />
                    </Form.Item>
                    <Form.Item className="form-add-pawn-item" label="Ngày cấp">
                        <ConfigProvider locale={viVN}>
                            <DatePicker
                                className="form-input-pawn"
                                onChange={handleChangeDateCCCD}
                                name="day_cccd"
                                locale={locale}
                            />
                        </ConfigProvider>
                    </Form.Item>
                    <Form.Item className="form-add-pawn-item" label="Nơi cấp" >
                        <Input
                            defaultValue={formData.address_cccd}
                            className="form-input-pawn"
                            onChange={handleChange}
                            name="address_cccd"
                        />
                    </Form.Item>
                    <Form.Item
                        className="form-add-pawn-item"
                        label="Số điện thoại"
                        name="phone"
                        rules={[
                            {
                                required: true,
                                message: 'Vui lòng nhập Số điện thoại',
                            },
                        ]}>
                        <Input
                            className="form-input-pawn"
                            onChange={handleChange}
                            name="phone"
                        />
                    </Form.Item>
                    <Form.Item className="form-add-pawn-item" label="Hộ khẩu thường trú">
                        <TextArea
                            className="form-input-textarea"
                            onChange={handleChange}
                            name="address"
                        />
                    </Form.Item>
                </Form>
            </>
        </Modal>
    </>
    )
}