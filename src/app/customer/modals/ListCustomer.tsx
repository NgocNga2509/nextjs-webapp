"use client"
import BaseURL from "@/app/api/BaseURL";
import { FormatDate } from "@/app/helpers/Helps";
import { Input, Modal, Pagination, Radio, Select, Table, Spin } from "antd";
import { useEffect, useState } from "react";
interface CustomerList{
    customer_name: "",
    customer_id: "",
    birth_date: "",
    gender: "",
    phone: "",
    cccd: "",
    address: "",
}
export default function ListCustomerModal({ isOpen, setIsOpen, setDataCustomer,setCustomerID, setIsSelect }: any) {
    const [loading, setLoading] = useState(true);
    const [data, setData] = useState<CustomerList[]>([]);
    const [isOpenModalOrder, setIsOpenModalOrder] = useState(false);
    const [selectSearch, setSelectSearch] = useState(0);
    const handleCancel = () => {
        setIsOpen(false)
    }
    const handleChangeSelectSearch = (e:any) => {
        setSelectSearch(e);
    };

    useEffect(()=>{
        BaseURL.get("list-customer").then((response)=>{
            if(response.status == 200){
                setData(response.data)
            }
        }).catch((error) => console.log(error))
    },[isOpen])
    const handleChangeSearch = (e:any) => {
        // setCurrentPage(1);
        // setLoading(true);
        // selectSearch == 1
        //     ? BaseURL
        //         .post("search-customer-by-phone", {
        //             phone: e.target.value,
        //         })
        //         .then((response) => {
        //             setLoading(false)
        //             setData(response.data);
        //         })
        //     : selectSearch == 2
        //         ? BaseURL
        //             .post("search-customer-by-cccd", {
        //                 cccd: e.target.value,
        //             })
        //             .then((response) => {
        //                 setLoading(false)
        //                 setData(response.data);
        //             })
        //         : BaseURL
        //             .post("search-customer-by-name", {
        //                 name: e.target.value,
        //             })
        //             .then((response) => {
        //                 setLoading(false)
        //                 setData(response.data);
        //             });
    };

    const handleOk = () => {
        // setIsSelect(true);
        // setIsOpen(false);
        // setNewCustomer(false);
        // setMessage("");
    };
    const convertGender = (gender: any) => {
        if( gender == ""){
          return ""
        }else if (gender === "0") {
          return "Nữ";
        } else {
          return "Nam";
        }
      };
    const dataSource =
        data.map((item, index) => ({
            key: index,
            CustomerName: item.customer_name,
            CustomerID: item.customer_id,
            BirthDate: item.birth_date,
            Gender: item.gender,
            Phone: item.phone,
            CCCD: item.cccd,
            Address: item.address,
        }));
    const [currentPage, setCurrentPage] = useState(1); // Trang hiện tại
    const pageSize = 5; // Kích thước trang
    // Số lượng tổng cộng các mục dữ liệu
    const totalItems = dataSource.length;
    const handlePageChange = (page: any) => {
        setCurrentPage(page);
    };
    // Lấy dữ liệu trang hiện tại
    const currentPageData = dataSource.slice(
        (currentPage - 1) * pageSize,
        currentPage * pageSize
    );
    const handleSelectCustomer = (record: any) =>{
        setDataCustomer(record)
        setCustomerID(record.CustomerID)
        setIsSelect(true)
        setIsOpen(false)
    }

    const columns = [
        {
            title: "ID",
            dataIndex: "CustomerID",
            key: "CustomerID",
        },
        {
            title: "Tên",
            dataIndex: "CustomerName",
            key: "CustomerName",
            render: (name:string,record: any) => <div className="name-customer" onClick={()=>handleSelectCustomer(record)}>{name}</div>
        },
        {
            title: "Ngày tháng năm sinh",
            dataIndex: "BirthDate",
            key: "BirthDate",
            render: (date:any) => date && <FormatDate date={date} />,
        },
        {
            title: "SĐT",
            dataIndex: "Phone",
            key: "Phone",
        },
        {
            title: "Giới tính",
            dataIndex: "Gender",
            key: "Gender",
            render: (gender:any) => convertGender(gender),
        },
        {
            title: "CMND/CCCD",
            dataIndex: "CCCD",
            key: "CCCD",
        },
        {
            title: "Địa chỉ",
            dataIndex: "Address",
            key: "Address",
        },
        {
            title: "Thông tin giao dịch",
            dataIndex: "CustomerID",
            key: "CustomerID",
            render: (id: number) => (
                <span
                    className="link-detail"
                // onClick={() => {
                //     setIsOpenModalOrder(true);
                //     setCustomerID(id);
                // }}
                >
                    Xem chi tiết
                </span>
            ),
        },
    ];

    const [selectedRowKeys, setSelectedRowKeys] = useState([]);

    const onChange = (selectedRowKeys: any) => {
        setSelectedRowKeys(selectedRowKeys);
        const selectedRows = dataSource.filter((row) =>
            selectedRowKeys.includes(row.key)
        );
        //setDataCustomer(selectedRows[0]);
    };

    const rowSelection = {
        selectedRowKeys,
        onChange: onChange,
        type: "radio",
    };

    return (
        <div className="content-modal">
            <Modal
                className="modal-list-customer"
                title="Danh sách khách hàng"
                open={isOpen}
                onCancel={handleCancel}
                onOk={handleOk}
                zIndex={1030}
                width={1000}
                bodyStyle={{ maxHeight: '800px', overflow: 'auto' }}
            >
                <div className="search-customer">
                    <Input
                        type="text"
                        //value=""
                        className="form-control"
                        placeholder="Tìm kiếm..."
                        onChange={handleChangeSearch}
                    />
                    <div>
                        <Select
                            defaultValue={0}
                            onChange={(e) => handleChangeSelectSearch(e)}
                            className="select-search"
                        >
                            <Select.Option value={0}>Tên</Select.Option>
                            <Select.Option value={1}>Số điện thoại</Select.Option>
                            <Select.Option value={2}>CMND/CCCD</Select.Option>
                        </Select>
                    </div>
                </div>
                {/* {loading ? (
                    <Spin size="large" /> // Hiển thị loading khi loading = true
                ) : */}
                    <>
                        <Table
                            className="table-list-customer"
                            dataSource={currentPageData}
                            columns={columns}
                            // rowSelection={rowSelection}
                            pagination={false}
                        />
                        <Pagination showSizeChanger={false}
                            className="pagination-category"
                            current={currentPage}
                            pageSize={pageSize}
                            total={totalItems}
                            onChange={handlePageChange}
                        />
                    </>
                {/* } */}
            </Modal>
            {/* {isOpenModalOrder && (
          <ModalOrder
            isOpen={isOpenModalOrder}
            setIsOpen={setIsOpenModalOrder}
            customerID={customerID}
          />
        )} */}
        </div>
    )
}
